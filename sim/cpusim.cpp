/*
 * Dramite
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "cpusim.h"
#include "cpucore.h"
#include "cpubus.h"

//#define VERBOSE

CPUSim::CPUSim() {
    _bus_state = BUS_IDLE;
    _last_reset = 0;
    m_bus_request = false;
    m_bus_req_wr = false;
    m_bus_req_mio = false;
    m_bus_completed = false;
    m_bus_req_addr = 0;
    m_bus_req_data = 0;
    m_bus_active = false;

    m_cpubus = new CPUBUS(m_bus_request, m_bus_completed, m_bus_req_addr,
        m_bus_req_data, m_bus_req_be, m_bus_req_wr, m_bus_req_mio, m_cycles);
}

CPUSim::~CPUSim() {
    //delete m_cpucore;
    //delete m_cpubus;
}

void CPUSim::start() {
    // Nothing
}

void CPUSim::apply(
        uint32_t &cpu_a,
        uint32_t cpu_d_out,
        uint32_t &cpu_d_in,
        uint8_t &cpu_ads_n,
        uint8_t &cpu_be_n,
        const uint8_t cpu_busy_n,
        const uint8_t cpu_clk2,
        uint8_t &cpu_dc,
        const uint8_t cpu_error_n,
        uint8_t &cpu_lock_n,
        const uint8_t cpu_intr,
        uint8_t &cpu_mio,
        const uint8_t cpu_nmi,
        const uint8_t cpu_pereq,
        const uint8_t cpu_ready_n,
        const uint8_t cpu_reset,
        uint8_t &cpu_wr) {
        
    // Currently it doens't utilize the clk2 signal, assuming the function
    // is called each time before a clk2 low to high transition

    if ((!_last_reset)&&(cpu_reset)) {
        // Reset bus signals
        cpu_a = 0x0;
        cpu_d_in = 0x0;
        cpu_ads_n = true;
        cpu_be_n = 0xf;
        cpu_dc = true;
        cpu_lock_n = true;
        cpu_mio = true;
        cpu_wr = false;

        m_bus_active = false;
    }
    else {
        switch (_bus_state) {
            case BUS_IDLE: {
                // Currently Idle, check if there is new request pending
                if (m_bus_request) {
                    // Update internal status
                    _bus_req_addr = m_bus_req_addr;
                    _bus_req_data = m_bus_req_data;
                    _bus_req_be = m_bus_req_be;
                    _bus_req_wr = m_bus_req_wr;
                    _bus_req_mio = m_bus_req_mio;
                    #ifdef VERBOSE
                    printf("cpusim: bus %s %c %04x %02x\n", _bus_req_mio ? "mem":"io", _bus_req_wr ? 'W':'R', _bus_req_addr, _bus_req_data);
                    #endif
                    _bus_state = BUS_C1_T2;
                    m_bus_active = true;
                    m_cycles = 0;
                    // Put signals on the bus
                    // Cycle 1, non pipelined, T1
                    cpu_a = _bus_req_addr >> 2;
                    cpu_be_n = (~_bus_req_be) & 0xf;
                    cpu_mio = _bus_req_mio;
                    cpu_dc = true; // So far only data request
                    cpu_wr = _bus_req_wr;
                    cpu_ads_n = false;
                    cpu_lock_n = true;
                }
                break;
            }
            case BUS_C1_T2: {
                // Cycle 1, non pipelined, T2
                _bus_state = BUS_C2_T1;
                m_cycles++;
                if (_bus_req_wr == REQ_WR)
                    cpu_d_in = _bus_req_data;
                break;
            }
            case BUS_C2_T1: {
                cpu_ads_n = true;
                _bus_state = BUS_C2_T2;
                m_cycles++;
                break;
            }
            case BUS_C2_T2: {
                _bus_state = BUS_DONE;
                m_cycles++;
                break;
            }
            case BUS_DONE: {
                if (cpu_ready_n == false) {
                    // Transaction finished
                    if (_bus_req_wr == REQ_RD)
                        m_bus_req_data = cpu_d_out;
                    // next state will be idle
                    _bus_state = BUS_IDLE;
                    m_bus_active = false;
                    // Signal CPU thread
                    m_bus_completed = true;
                    while (m_bus_request); // Wait ACK
                    m_bus_completed = false;
                }
                else {
                    _bus_state = BUS_C2_T1;
                }
                break;
            }
        }

    }

    _last_reset = cpu_reset;

}