/*
 * Dramite
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

class PS2sim {
public:
    PS2sim();
    ~PS2sim();
    void reset();
    void apply(uint8_t clk_en, uint8_t clk_out, uint8_t &clk_in,
        uint8_t dat_en, uint8_t dat_out, uint8_t &dat_in);
private:
    // Bus states
    uint8_t bit_counter;
    uint16_t sr;
    enum {
        PS2_IDLE, PS2_RX, PS2_TX 
    } state;
    int tick_counter;
    int tick_per_half_bit;
    // Private functions
    uint16_t handle_byte(uint16_t rx);
    uint8_t txbuf[3];
    uint8_t txcnt;
};
