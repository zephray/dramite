/*
 * Dramite
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include "avalonmem.h"

AvalonMem::AvalonMem(uint32_t base, uint32_t size) {
    this->base = base;
    this->size = size;
    current_req.beatcount = 0;
    this->mem = (uint32_t *)malloc(size);
}

AvalonMem::~AvalonMem() {
    free(this->mem);
}

void AvalonMem::reset() {
    current_req.beatcount = 0;
}

void AvalonMem::apply(uint32_t address, uint8_t byteenable, uint8_t read,
        uint32_t &readdata, uint8_t write, uint32_t writedata,
        uint8_t &waitrequest, uint8_t &readdatavalid, uint8_t burstcount) {
    // Called during every posedge
    // Set default outputs
    waitrequest = 0;
    readdata = 0;
    readdatavalid = 0;

    // Can't both write and ready at the same cycle
    assert(!(read && write));

    // Handle new requests
    if ((current_req.beatcount == 0) && (read || write)) {
        current_req.addr = address;
        current_req.burst = burstcount;
        current_req.iswrite = write;
        #ifdef VERBOSE
        fprintf(stderr, "MEM: %c %08x BURST %d\n", write ? 'W' : 'R', address,
                burstcount);
        #endif
        current_req.beatcount = burstcount;
        waitrequest = 1;
    }
    // Process request
    if (current_req.beatcount != 0) {
        if (current_req.iswrite) {
            waitrequest = 0;
            if (write) {
                this->write(current_req.addr, writedata, byteenable);
                current_req.beatcount--;
                current_req.addr += 4;
            }
        }
        else {
            waitrequest = 1;
            readdatavalid = 1;
            readdata = this->read(current_req.addr);
            current_req.beatcount--;
            current_req.addr += 4;
        }
    }
}

uint32_t AvalonMem::get_bitmask(uint8_t mask) {
    uint32_t bm = 0;
    for (int i = 0; i < 4; i++) {
        if (mask & 0x01)
            bm |= (0xffull << i * 8);
        mask >>= 1;
    }
    return bm;
}

uint32_t AvalonMem::read(uint32_t addr) {
    uint32_t original_addr = addr;
    addr -= this->base;
    if (addr > this->size) {
        fprintf(stderr, "Out of bound read from addr %08x\n", original_addr);
        return 0;
    }
    addr >>= 2;

    return mem[addr];
}

void AvalonMem::write(uint32_t addr, uint32_t data, uint8_t mask) {
    uint32_t original_addr = addr;
    addr -= this->base;
    if (addr > this->size) {
        fprintf(stderr, "Out of bound write to addr %08x\n", original_addr);
        return;
    }
    addr >>= 2;

    if (mask == 0xf) {
        mem[addr] = data;
    }
    else {
        uint32_t d = mem[addr];
        uint32_t bm = get_bitmask(mask);
        d &= ~bm;
        d |= data & bm;
        mem[addr] = d;
    }
}

void AvalonMem::load(const char *fn, size_t offset) {
    FILE *fp;
    fp = fopen(fn, "rb+");
    if (!fp) {
        fprintf(stderr, "Error: unable to open file %s\n", fn);
        exit(1);
    }
    fseek(fp, 0, SEEK_END);
    size_t fsize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    size_t result = fread((void *)((size_t)mem + offset), fsize, 1, fp);
    assert(result == 1);
    fclose(fp);
}
