/*
 * Dramite
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

class AvalonMem {
public:
    AvalonMem(uint32_t base, uint32_t size);
    ~AvalonMem();
    void reset();
    void apply(uint32_t address, uint8_t byteenable, uint8_t read,
        uint32_t &readdata, uint8_t write, uint32_t writedata,
        uint8_t &waitrequest, uint8_t &readdatavalid, uint8_t burstcount);
    void load(const char *fn, size_t offset);
private:
    typedef struct {
        // Request
        uint8_t iswrite;
        uint32_t addr;
        uint8_t burst;
        // State
        int beatcount;
    } req_t;

    // Current processing request
    req_t current_req;

    uint32_t base;
    uint32_t size;

    uint32_t get_bitmask(uint8_t mask);
    uint32_t read(uint32_t addr);
    void write(uint32_t addr, uint32_t data, uint8_t mask);

    uint32_t *mem;
};
