/*
 * Dramite
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <SDL.h>

class DispSim {
public:    
    const int contentWidth = 640;
    const int contentHeight = 480;
    const int dispWidth = 640;
    const int dispHeight = 480;
    DispSim(void);
    ~DispSim(void);
    void apply(const uint8_t vga_clk, const uint8_t vga_hs,
        const uint8_t vga_vs, const uint8_t vga_r, const uint8_t vga_g,
        const uint8_t vga_b, const uint8_t vga_blank_n);
    void set_title(char *title);
private:
    static constexpr int HBP = 48;
    static constexpr int VBP = 33;
    static constexpr int REFRESH_INTERVAL = 20;
    SDL_Surface       *screen           = NULL;
    SDL_Window        *window           = NULL;
    SDL_Renderer      *renderer         = NULL;
    SDL_Texture       *texture          = NULL;
    SDL_Rect           textureRect;
    uint8_t last_clk;
    uint8_t last_vs;
    uint8_t last_hs;
    int x_counter;
    int y_counter;
    int tick;
    void render_copy(void);
    void set_pixel(int x, int y, uint32_t pixel);
};
