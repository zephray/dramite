#pragma once

#define CLK_PREIOD_PS       (20000) // 50 MHz
#define CLK_HALF_PERIOD_PS  (CLK_PREIOD_PS / 2)

#define RAM_SIZE            (16*1024*1024)