#
# Dramite
# Copyright 2023 Wenting Zhang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

TARGET ?= system
all: $(TARGET)

VOBJ := obj_dir
CXX   := g++
FBDIR := ../rtl
CPUS ?= $(shell bash -c 'nproc --all')
VERBOSE ?= 0

.PHONY: all
$(TARGET): $(VOBJ)/V$(TARGET)__ALL.a

SUBMAKE := $(MAKE) --no-print-directory --directory=$(VOBJ) -f
ifeq ($(VERILATOR_ROOT),)
VERILATOR := verilator
else
VERILATOR := $(VERILATOR_ROOT)/bin/verilator
endif
VFLAGS := -Wall -Wno-fatal -Wno-TIMESCALEMOD -Wno-PINCONNECTEMPTY -MMD --trace -cc \
		-I../rtl/ao486 \
		-I../rtl/ao486/memory \
		-I../rtl/ao486/pipeline \
		-I../rtl/basic \
		-I../rtl/bus \
		-I../rtl/common \
		-I../rtl/soc/driver_sd \
		-I../rtl/soc/driver_sound \
		-I../rtl/soc/floppy \
		-I../rtl/soc/hdd \
		-I../rtl/soc/pc_bus \
		-I../rtl/soc/pc_dma \
		-I../rtl/soc/pic \
		-I../rtl/soc/pit \
		-I../rtl/soc/ps2 \
		-I../rtl/soc/rtc \
		-I../rtl/soc/sound \
		-I../rtl/soc/vga \
		-I../rtl
ifeq ($(VERBOSE), 1)
VFLAGS += +define+VERBOSE=1
endif

$(VOBJ)/V$(TARGET)__ALL.a: $(VOBJ)/V$(TARGET).cpp $(VOBJ)/V$(TARGET).h
$(VOBJ)/V$(TARGET)__ALL.a: $(VOBJ)/V$(TARGET).mk

$(VOBJ)/V%.cpp $(VOBJ)/V%.h $(VOBJ)/V%.mk: $(FBDIR)/%.v
	$(VERILATOR) $(VFLAGS) $*.v

$(VOBJ)/V%.cpp: $(VOBJ)/V%.h
$(VOBJ)/V%.mk:  $(VOBJ)/V%.h
$(VOBJ)/V%.h: $(FBDIR)/%.v

$(VOBJ)/V%__ALL.a: $(VOBJ)/V%.mk
	$(SUBMAKE) V$*.mk -j$(CPUS)

.PHONY: clean
clean:
	rm -rf $(VOBJ)/*.mk
	rm -rf $(VOBJ)/*.cpp
	rm -rf $(VOBJ)/*.h
	rm -rf $(VOBJ)/
