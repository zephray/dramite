/*
 * Dramite
 * Copyright 2021 Marco Bortolin (part of IBMulator project)
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "common.h"
#include "../cpubus.h"
#include "memory.h"

Memory g_memory;

Memory::Memory() {
    // nothing
}

Memory::~Memory() {
    // nothing
}

void Memory::init(CPUBUS *bus) {
    cpubus = bus;
}

template<>
uint32_t Memory::read_mapped<1>(uint32_t _addr, int &_cycles) const noexcept
{
    return (uint32_t)cpubus->read_uint8(_addr, REQ_MEM, _cycles);
}

template<>
uint32_t Memory::read_mapped<2>(uint32_t _addr, int &_cycles) const noexcept
{
    return (uint32_t)cpubus->read_uint16(_addr, REQ_MEM, _cycles);
}

template<>
uint32_t Memory::read_mapped<4>(uint32_t _addr, int &_cycles) const noexcept
{
    return cpubus->read_uint32(_addr, REQ_MEM, _cycles);
}

template<>
void Memory::write_mapped<1>(uint32_t _addr, uint32_t _data, int &_cycles) noexcept
{
    cpubus->write_uint8(_addr, _data, REQ_MEM, _cycles);
}

template<>
void Memory::write_mapped<2>(uint32_t _addr, uint32_t _data, int &_cycles) noexcept
{
    cpubus->write_uint16(_addr, _data, REQ_MEM, _cycles);
}

template<>
void Memory::write_mapped<4>(uint32_t _addr, uint32_t _data, int &_cycles) noexcept
{
    cpubus->write_uint32(_addr, _data, REQ_MEM, _cycles);
}

void Memory::check_trap(uint32_t _address, uint8_t _mask, uint32_t _value, unsigned _len)
const noexcept
{
    //
}

uint8_t  Memory::dbg_read_byte(uint32_t _addr) const noexcept
{
	return 0xFF;
}

uint16_t Memory::dbg_read_word(uint32_t _addr) const noexcept
{
    return 0xFFFF;
}

uint32_t Memory::dbg_read_dword(uint32_t _addr) const noexcept
{
    return 0xFFFFFFFF;
}

uint64_t Memory::dbg_read_qword(uint32_t _addr) const noexcept
{
	return (
		uint64_t(dbg_read_dword(_addr)) | uint64_t(dbg_read_dword(_addr+4)) << 32
	);
}

uint32_t *Memory::get_buffer_ptr(uint32_t _addr)
{
    assert(0);
    return NULL;
}

Devices g_devices;

Devices::Devices() {
    //
}

Devices::~Devices() {
    //
}

void Devices::init(CPUBUS *bus) {
    cpubus = bus;
}

uint8_t Devices::read_byte(uint16_t _port) {
    return cpubus->read_uint8(_port, REQ_IO, io_cycles);
}

uint16_t Devices::read_word(uint16_t _port) {

    return cpubus->read_uint16(_port, REQ_IO, io_cycles);
}

uint32_t Devices::read_dword(uint16_t _port) {
    return cpubus->read_uint32(_port, REQ_IO, io_cycles);
}

void Devices::write_byte(uint16_t _port, uint8_t _value) {
    cpubus->write_uint8(_port, _value, REQ_IO, io_cycles);
}

void Devices::write_word(uint16_t _port, uint16_t _value) {
    cpubus->write_uint16(_port, _value, REQ_IO, io_cycles);
}

void Devices::write_dword(uint16_t _port, uint32_t _value) {
    cpubus->write_uint32(_port, _value, REQ_IO, io_cycles);
}

int Devices::get_io_time() {
    int io_time = io_cycles;
    io_cycles = 0;
    return io_time;
}