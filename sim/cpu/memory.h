/*
 * Dramite
 * Copyright 2021 Marco Bortolin (part of IBMulator project)
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "common.h"

// Redirect memory and IO port access to bus master
class Memory;
extern Memory g_memory;

class CPUBUS;

#define MEM_TRAP_READ  0x1
#define MEM_TRAP_WRITE 0x2

class Memory
{
public:
    Memory();
    ~Memory();

    void init(CPUBUS *bus);

    void check_trap(uint32_t _address, uint8_t _mask, uint32_t _value, unsigned _len) const noexcept;

	// read functions for CPUBus
	template<unsigned LEN> inline
	uint32_t read(uint32_t _address, int &_cycles) const noexcept
	{
		return read_mapped<LEN>(_address, _cycles);
	}
	template<unsigned LEN> inline
	uint32_t read_t(uint32_t _address, unsigned _trap_len, int &_cycles) const noexcept
	{
		#if MEMORY_TRAPS
		uint32_t value = read_mapped<LEN>(_address, _cycles);
		check_trap(_address, MEM_TRAP_READ, value, _trap_len);
		return value;
		#else
		UNUSED(_trap_len);
		return read_mapped<LEN>(_address, _cycles);
		#endif
	}
	template<unsigned LEN>
	uint32_t read_mapped(uint32_t _address, int &_cycles) const noexcept { assert(false); return ~0; }


	// write functions for CPUBus
	template<unsigned LEN>
	void write(uint32_t _addr, uint32_t _data, int &_cycles) noexcept
	{
		write_mapped<LEN>(_addr, _data, _cycles);
	}
	template<unsigned LEN> inline
	void write_t(uint32_t _addr, uint32_t _data, unsigned _trap_len, int &_cycles) noexcept
	{
		write_mapped<LEN>(_addr, _data, _cycles);
		#if MEMORY_TRAPS
		check_trap(_addr, MEM_TRAP_WRITE, _data, _trap_len);
		#else
		UNUSED(_trap_len);
		#endif
	}
	template<unsigned LEN>
	void write_mapped(uint32_t _addr, uint32_t _data, int &_cycles) noexcept { assert(false); }

	uint8_t  dbg_read_byte (uint32_t _addr) const noexcept;
	uint16_t dbg_read_word (uint32_t _addr) const noexcept;
	uint32_t dbg_read_dword(uint32_t _addr) const noexcept;
	uint64_t dbg_read_qword(uint32_t _addr) const noexcept;
	uint32_t *get_buffer_ptr(uint32_t _addr);
private:
    CPUBUS *cpubus;
};

template<> uint32_t Memory::read_mapped<1>(uint32_t _addr, int &_cycles) const noexcept;
template<> uint32_t Memory::read_mapped<2>(uint32_t _addr, int &_cycles) const noexcept;
template<> uint32_t Memory::read_mapped<4>(uint32_t _addr, int &_cycles) const noexcept;

template<> void Memory::write_mapped<1>(uint32_t _addr, uint32_t _data, int &_cycles) noexcept;
template<> void Memory::write_mapped<2>(uint32_t _addr, uint32_t _data, int &_cycles) noexcept;
template<> void Memory::write_mapped<4>(uint32_t _addr, uint32_t _data, int &_cycles) noexcept;

class Devices;
extern Devices g_devices;

class Devices
{
public:
    Devices();
    ~Devices();

    void init(CPUBUS *bus);

    uint8_t read_byte(uint16_t _port);
	uint16_t read_word(uint16_t _port);
	uint32_t read_dword(uint16_t _port);
	void write_byte(uint16_t _port, uint8_t _value);
	void write_word(uint16_t _port, uint16_t _value);
	void write_dword(uint16_t _port, uint32_t _value);

	int get_io_time();
private:
    CPUBUS *cpubus;
	int io_cycles;
};
