/*******************************************************************************
    This program is free software (firmware): you can redistribute it and/or
    modify it under the terms of  the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or (at
    your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program.  (It's in the $(ROOT)/doc directory.  Run make with no
    target there if the PDF file isn't present.)  If not, see
    <http://www.gnu.org/licenses/> for a copy.

    Copyright (C) 2021 Wenting Zhang
    Copyright (C) 2021 Marco Bortolin (part of IBMulator project)
*******************************************************************************/
#pragma once

#include <cstdint>
#include <cstdlib>
#include <ctime>
#include <sys/types.h>
#include <cstdio>
#include <stdexcept>
#include <assert.h>

#if defined(__GNUC__)
	// both gcc and clang define __GNUC__
	#define GCC_ATTRIBUTE(x) __attribute__ ((x))
	#define LIKELY(x)    __builtin_expect((x),1)
	#define UNLIKELY(x)  __builtin_expect((x),0)
	#define ALWAYS_INLINE GCC_ATTRIBUTE(always_inline)
#else
	#error unsupported compiler
#endif

#define UNUSED(x) ((void)x)

#define PINFO(verb,format,...)	fprintf(stdout, format, ## __VA_ARGS__)
#define PWARN(format,...)		fprintf(stdout, format, ## __VA_ARGS__)
#define PERR(format,...)		fprintf(stderr, format, ## __VA_ARGS__)

#define PINFOF(verb,fac,format,...)	fprintf(stdout, format, ## __VA_ARGS__)
#define PWARNF(verb,fac,format,...)	fprintf(stdout, format, ## __VA_ARGS__)
#define PERRF(fac,format,...)		fprintf(stderr, format, ## __VA_ARGS__)

#define PERRFEX(fac,format,...) { fprintf(stderr, "%s:%d " format, __FILE__,__LINE__, ## __VA_ARGS__) }

#define PERR_ABORT(format,...) 		{ fprintf(stderr, format, ## __VA_ARGS__) ; exit(1); }
#define PERRF_ABORT(fac,format,...) { fprintf(stderr, format, ## __VA_ARGS__) ; exit(1); }
#define PERRFEX_ABORT(fac,format,...) { fprintf(stderr, "%s:%d " format, __FILE__,__LINE__, ## __VA_ARGS__) ; exit(1); }

#define PDEBUGEX(verb,format,...)        fprintf(stdout, format, ## __VA_ARGS__)
#define PDEBUG(verb,format,...)          fprintf(stdout, format, ## __VA_ARGS__)
#define PDEBUGEXF(verb,fac,format,...)   fprintf(stdout, format, ## __VA_ARGS__)
#define PDEBUGF(verb,fac,format,...)     fprintf(stdout, format, ## __VA_ARGS__)

void print_trace(void);

enum MachineReset {
	MACHINE_POWER_ON,   // Machine is switched on using the power button
	MACHINE_HARD_RESET, // Machine RESET triggered by the reset button
	                    //FIXME right now is equivalent to POWER_ON, should we remove it?
	CPU_SOFT_RESET,     // CPU RESET triggered by software
	DEVICE_SOFT_RESET   // Device RESET triggered by software
};