/*
 * Dramite
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdint.h>
#include <mutex>
#include <thread>
#include <condition_variable>

class CPU;
class CPUBUS;

class CPUSim {
private:
    typedef enum bus_state {
        BUS_IDLE,
        BUS_C1_T2,
        BUS_C2_T1,
        BUS_C2_T2,
        BUS_DONE
    } BusState;

    uint32_t _bus_req_addr;
    uint32_t _bus_req_data;
    bool _bus_req_dc;
    bool _bus_req_wr;
    bool _bus_req_mio;
    uint8_t _bus_req_be;
    BusState _bus_state;
    uint8_t _last_reset;

public:
    bool m_bus_active;
    CPUBUS *m_cpubus;
    volatile bool m_bus_request;
    volatile bool m_bus_completed; 
    volatile uint32_t m_bus_req_addr;
    volatile uint32_t m_bus_req_data;
    volatile uint8_t m_bus_req_be;
    volatile bool m_bus_req_wr;
    volatile bool m_bus_req_dc;
    volatile bool m_bus_req_mio;
    int m_cycles;

    CPUSim();
    ~CPUSim();
    void start();
    void apply(uint32_t &cpu_a, uint32_t cpu_d_out, uint32_t &cpu_d_in,
        uint8_t &cpu_ads_n, uint8_t &cpu_be_n, const uint8_t cpu_busy_n,
        const uint8_t cpu_clk2, uint8_t &cpu_dc, const uint8_t cpu_error_n,
        uint8_t &cpu_lock_n, const uint8_t cpu_intr, uint8_t &cpu_mio,
        const uint8_t cpu_nmi, const uint8_t cpu_pereq,
        const uint8_t cpu_ready_n, const uint8_t cpu_reset, uint8_t &cpu_wr);
};
