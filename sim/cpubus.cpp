/*
 * Dramite
 * Copyright 2018 Kanta Mori (part of tiny x86 cpu emulator project)
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <functional>
#include "cpubus.h"

#include "cpucore.h"

//#define VERBOSE

// This implementation currently assumes DX bus (32 bit) and little endian

uint32_t CPUBUS::bus_transaction(uint32_t addr, uint32_t data, uint8_t be,
        bool wr, bool mio, int& cycles) {

    m_bus_req_addr = addr;
    m_bus_req_data = data;
    m_bus_req_be = be;
    m_bus_req_wr = wr;
    m_bus_req_mio = mio;
    m_bus_request = true;

    while (!m_bus_completed);
    m_bus_request = false;
    while (m_bus_completed);

    cycles += m_cycles;
    //cycles += 1;
    return m_bus_req_data;
}

uint8_t CPUBUS::rw_uint8(uint32_t addr, uint8_t data, bool wr, bool mio,
        int& cycles) {
    uint32_t addr_aligned = addr & ~0x3;
    switch (addr & 0x3) {
    case 0:
        return (uint8_t)((bus_transaction(addr_aligned, (uint32_t)data,
                0x1, wr, mio, cycles)) & 0xff);
    case 1:
        return (uint8_t)((bus_transaction(addr_aligned, (uint32_t)data << 8,
                0x2, wr, mio, cycles) >> 8) & 0xff);
    case 2:
        return (uint8_t)((bus_transaction(addr_aligned, (uint32_t)data << 16,
                0x4, wr, mio, cycles) >> 16) & 0xff);
    case 3:
        return (uint8_t)((bus_transaction(addr_aligned, (uint32_t)data << 24,
                0x8, wr, mio, cycles) >> 24) & 0xff);
    }
    return 0; // never reaches
}

uint16_t CPUBUS::rw_uint16(uint32_t addr, uint16_t data, bool wr, bool mio,
        int& cycles) {
    uint32_t addr_aligned = addr & ~0x3;
    uint16_t d;
    switch (addr & 0x3) {
    case 0:
        return (uint16_t)((bus_transaction(addr_aligned, (uint32_t)data,
                0x3, wr, mio, cycles)) & 0xffff);
    case 1:
        return (uint16_t)((bus_transaction(addr_aligned, (uint32_t)data << 8,
                0x6, wr, mio, cycles) >> 8) & 0xffff);
    case 2:
        return (uint16_t)((bus_transaction(addr_aligned, (uint32_t)data << 16,
                0xc, wr, mio, cycles) >> 16) & 0xffff);
    case 3:
        // Cross 32-bit boundary, need 2 accesses
        d = rw_uint8(addr, data & 0xff, wr, mio, cycles);
        d |= (uint16_t)rw_uint8(addr + 1, (data >> 8) & 0xff, wr, mio, cycles)
                << 8;
        return d; 
    }
    return 0; // never reaches
}

uint32_t CPUBUS::rw_uint32(uint32_t addr, uint32_t data, bool wr, bool mio,
        int& cycles) {
    uint32_t addr_aligned = addr & ~0x3;
    uint32_t d;
    switch (addr & 0x3) {
    case 0:
        return bus_transaction(addr_aligned, data,
                0xf, wr, mio, cycles);
    case 1:
        d = (bus_transaction(addr_aligned, data << 8,
                0xe, wr, mio, cycles) >> 8) & 0xffffff;
        d |= (uint32_t)rw_uint8(addr + 3, data >> 24, wr, mio, cycles) << 24;
        return d;
    case 2:
        d = rw_uint16(addr, data, wr, mio, cycles);
        d |= (uint32_t)rw_uint16(addr + 2, data >> 16, wr, mio, cycles) << 16;
        return d;
    case 3:
        d = rw_uint8(addr, data, wr, mio, cycles);
        d |= (uint32_t)bus_transaction(addr_aligned, data >> 8,
                0x7, wr, mio, cycles) << 8;
        return d; 
    }
    return 0; // never reaches
}

uint8_t CPUBUS::read_uint8(uint32_t addr, bool mio, int& cycles) {
    uint8_t data = rw_uint8(addr, 0, REQ_RD, mio, cycles);
    #ifdef VERBOSE
    printf("Access 8bit data %08x, got %02x\n", addr, data);
    #endif
    return data;
}

uint16_t CPUBUS::read_uint16(uint32_t addr, bool mio, int& cycles) {
    uint16_t data = rw_uint16(addr, 0, REQ_RD, mio, cycles);
    #ifdef VERBOSE
    printf("Access 16bit data %08x, got %04x\n", addr, data);
    #endif
    return data;
}

uint32_t CPUBUS::read_uint32(uint32_t addr, bool mio, int& cycles) {
    uint32_t data = rw_uint32(addr, 0, REQ_RD, mio, cycles);
    #ifdef VERBOSE
    printf("Access 32bit data %08x, got %08x\n", addr, data);
    #endif
    return data;
}

void CPUBUS::write_uint8(uint32_t addr, uint8_t data, bool mio, int& cycles) {
    rw_uint8(addr, data, REQ_WR, mio, cycles);
}

void CPUBUS::write_uint16(uint32_t addr, uint16_t data, bool mio, int& cycles) {
    rw_uint16(addr, data, REQ_WR, mio, cycles);
}

void CPUBUS::write_uint32(uint32_t addr, uint32_t data, bool mio, int& cycles) {
    rw_uint32(addr, data, REQ_WR, mio, cycles);
}
