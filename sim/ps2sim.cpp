/*
 * Dramite
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "config.h"
#include "ps2sim.h"

//#define VERBOSE

//#define PS2_TX

// Will only query keyboard input every TX_DIV cycles
#define TX_DIV  2000

#define TICK_PER_BIT        (int)(1.0e12 / 12000 / CLK_PREIOD_PS)
#define TICK_PER_HALF_BIT   (TICK_PER_BIT / 2)

PS2sim::PS2sim() { 

    #ifdef PS2_TX
    struct termios t;
    tcgetattr(0, &t);
    t.c_lflag &= ~(ECHO | ICANON);
    tcsetattr(0, TCSANOW, &t);
    fcntl(0, F_SETFL, O_NONBLOCK);
    #endif
    reset();
}

PS2sim::~PS2sim() {
    #ifdef PS2_TX
    struct termios t;
    tcgetattr(0, &t);
    t.c_lflag |= (ECHO | ICANON);
    tcsetattr(0, TCSANOW, &t);
    fcntl(0, F_SETFL, O_NONBLOCK);
    #endif
}

void PS2sim::reset() {
    bit_counter = 0;
    tick_counter = 0;
    state = PS2_IDLE;
    txcnt = 0;
}

void PS2sim::apply(uint8_t clk_en, uint8_t clk_out, uint8_t &clk_in,
        uint8_t dat_en, uint8_t dat_out, uint8_t &dat_in) {

    // Default pull up
    if (!clk_en)
        clk_in = 1;
    if (!dat_en)
        dat_in = 1;

    if (state == PS2_IDLE) {
        if (!clk_en && dat_en && !dat_out) {
            // HOST RTS, switch to RX mode
            state = PS2_RX;
            bit_counter = 0;
            tick_counter = 0;
            sr = 0;
        }
    }
    else {
        // TX or RX in progress
        // Set dat line
        if (state == PS2_RX) {
            if (bit_counter == 10) {
                dat_in = 0;
            }
        }
        else {
            dat_in = sr & 0x01;
        }
        // Set clk line
        if (tick_counter < TICK_PER_HALF_BIT) {
            clk_in = 0;
            tick_counter++;
        }
        else if (tick_counter < TICK_PER_BIT) {
            clk_in = 1;
            tick_counter++;
        }
        else {
            // Falling edge
            tick_counter = 0;
            bit_counter ++;
            clk_in = 1;
            if (state == PS2_RX) {
                // Sample data here, probably good enough
                sr |= (dat_out) ? 0x1000 : 0x0;
                sr >>= 1;
                if (bit_counter == 12) {
                    // Finish
                    state = PS2_IDLE;
                    #ifdef VERBOSE
                    printf("PS2 RX: %04x\n", sr);
                    #endif
                    sr = handle_byte(sr);
                    if (sr != 0x00) {
                        #ifdef VERBOSE
                        printf("PS2 TX: %04x\n", sr);
                        #endif
                        state = PS2_TX;
                    }
                    bit_counter = 0;
                }
            }
            else {
                // TX
                sr >>= 1;
                if (bit_counter == 11) {
                    state = PS2_IDLE;
                    #ifdef VERBOSE
                    printf("PS2 TX DONE\n");
                    #endif
                    sr = handle_byte(0x00); // See if more bytes will be sent
                    if (sr != 0x00) {
                        #ifdef VERBOSE
                        printf("PS2 TX: %04x\n", sr);
                        #endif
                        state = PS2_TX;
                    }
                    bit_counter = 0;
                }
            }
        }
    }
}

uint16_t PS2sim::handle_byte(uint16_t rx) {
    uint16_t byte = 0;

    rx = rx & 0xff;
    if (txcnt != 0) {
        // Resp for old req hasn't finished
        txcnt--;
        byte = txbuf[txcnt];
    }
    else {
        if (rx == 0xff) {
            // Self test, return ACK and test passed
            byte = 0xfa;
            txbuf[0] = 0xaa;
            txcnt = 1;
        }
        else if (rx == 0xf4) {
            // Enable keybaord
            byte = 0xfa;
        }
        else if (rx == 0xf5) {
            // Disable keyboard
            byte = 0xfa;
        }
    }

    // Calculate parity
    if (byte != 0x00) {
        int parity = 0;
        for (int i = 0; i < 8; i++) {
            parity += ((byte >> i) & 0x1);
        }
        parity = ~parity;
        parity &= 0x01;
        byte |= parity << 8;
        byte |= 0x1 << 9; // Last bits should be 1 (stop bits)
        byte = byte << 1;
    }

    return byte;
}
