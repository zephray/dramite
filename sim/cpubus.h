/*
 * Dramite
 * Copyright 2018 Kanta Mori (part of tiny x86 cpu emulator project)
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <mutex>
#include <condition_variable>

// Definition for wr
#define REQ_WR  true
#define REQ_RD  false
// Definition for mio
#define REQ_MEM true
#define REQ_IO  false

class CPUBUS {
public:
    volatile bool& m_bus_request;
    volatile bool& m_bus_completed;
    volatile uint32_t& m_bus_req_addr;
    volatile uint32_t& m_bus_req_data;
    volatile uint8_t& m_bus_req_be;
    volatile bool& m_bus_req_wr;
    volatile bool& m_bus_req_mio;
    volatile int& m_cycles;
    
    CPUBUS(volatile bool& bus_request, volatile bool& bus_completed,
        volatile uint32_t& bus_req_addr, volatile uint32_t& bus_req_data,
        volatile uint8_t& bus_req_be, volatile bool& bus_req_wr,
        volatile bool& bus_req_mio, volatile int& cycles):
            m_bus_request(bus_request), m_bus_completed(bus_completed),
            m_bus_req_addr(bus_req_addr), m_bus_req_data(bus_req_data),
            m_bus_req_be(bus_req_be), m_bus_req_wr(bus_req_wr),
            m_bus_req_mio(bus_req_mio), m_cycles(cycles) {};

    uint32_t bus_transaction(uint32_t addr, uint32_t data, uint8_t be, bool wr,
            bool mio, int& cycles);
    uint8_t read_uint8(uint32_t addr, bool mio, int& cycles);
    uint16_t read_uint16(uint32_t addr, bool mio, int& cycles);
    uint32_t read_uint32(uint32_t addr, bool mio, int& cycles);
    void write_uint8(uint32_t addr, uint8_t data, bool mio, int& cycles);
    void write_uint16(uint32_t addr, uint16_t data, bool mio, int& cycles);
    void write_uint32(uint32_t addr, uint32_t data, bool mio, int& cycles);

private:
    uint8_t rw_uint8(uint32_t addr, uint8_t data, bool wr, bool mio,
            int& cycles);
    uint16_t rw_uint16(uint32_t addr, uint16_t data, bool wr, bool mio,
            int& cycles);
    uint32_t rw_uint32(uint32_t addr, uint32_t data, bool wr, bool mio,
            int& cycles);
};
