/*
 * Dramite
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "dispsim.h"

DispSim::DispSim(void) {
    window = SDL_CreateWindow("Dramite Simulation", 
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            dispWidth, dispHeight, SDL_SWSURFACE);

    if (window == NULL) {
        fprintf(stderr, "Unable to create window\n");
        return;
    }

    renderer = SDL_CreateRenderer(window, -1, 
            SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (renderer == NULL)
    {
        fprintf(stderr, "Unable to create renderer\n");
        return;
    }

    screen = SDL_CreateRGBSurface(SDL_SWSURFACE, contentWidth, contentHeight, 32,
            0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);

    textureRect.x = textureRect.y = 0;
    textureRect.w = contentWidth; 
    textureRect.h = contentHeight;

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, 
            SDL_TEXTUREACCESS_STREAMING, contentWidth, contentHeight);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0");

    if (screen == NULL || texture == NULL)
    {
        fprintf(stderr, "Unable to allocate framebuffer or texture\n");
        return;
    }

    x_counter = 0;
    y_counter = 0;

    SDL_FillRect(screen, &textureRect, 0xFF0000FF);
    render_copy();
    
    tick = SDL_GetTicks();
    
    printf("Video initialized\n");
}

DispSim::~DispSim(void) {
    if (screen != NULL)
    {
        SDL_FreeSurface(screen);
    }

    if (texture)
    {
	    SDL_DestroyTexture(texture);
    }

    if (renderer)
    {
        SDL_DestroyRenderer(renderer);
    }

    if (window)
    {
        SDL_DestroyWindow(window);
    }
}

static uint32_t color_map(uint8_t r, uint8_t g, uint8_t b) {
    return 0xff000000 | ((uint32_t)r << 16) | ((uint32_t)g << 8) | (b);
}

void DispSim::apply(const uint8_t vga_clk, const uint8_t vga_hs,
        const uint8_t vga_vs, const uint8_t vga_r, const uint8_t vga_g,
        const uint8_t vga_b, const uint8_t vga_blank_n) {
    if (!last_hs && vga_hs) {
        x_counter = 0;
        y_counter ++;
    }
    if (!last_vs && vga_vs) {
        // Verical sync can happen at the same time.
        y_counter = 0;
    }
    if (!last_clk && vga_clk && vga_blank_n) {
        x_counter ++;
        set_pixel(x_counter - HBP, y_counter - VBP,
            color_map(vga_r, vga_g, vga_b));
    }

    last_vs = vga_vs;
    last_hs = vga_hs;

    if ((SDL_GetTicks() - tick) > REFRESH_INTERVAL) {
        render_copy();
        tick = SDL_GetTicks();
    }
}

void DispSim::set_title(char *title) {
    SDL_SetWindowTitle(window, title);
}

void DispSim::render_copy(void) {
	void *texturePixels;
	int texturePitch;

	SDL_LockTexture(texture, NULL, &texturePixels, &texturePitch);
	memset(texturePixels, 0, textureRect.y * texturePitch);
	uint8_t *pixels = (uint8_t *)texturePixels + textureRect.y * texturePitch;
	uint8_t *src = (uint8_t *)screen->pixels;
	int leftPitch = textureRect.x << 2;
	int rightPitch = texturePitch - ((textureRect.x + textureRect.w) << 2);
	for (int y = 0; y < textureRect.h; y++, src += screen->pitch)
	{
		memset(pixels, 0, leftPitch); pixels += leftPitch;
		memcpy(pixels, src, contentWidth << 2); pixels += contentWidth << 2;
		memset(pixels, 0, rightPitch); pixels += rightPitch;
	}
	memset(pixels, 0, textureRect.y * texturePitch);
	SDL_UnlockTexture(texture);

	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer);
}

void DispSim::set_pixel(int x, int y, uint32_t pixel) {
    uint32_t *pixels = (uint32_t *)screen->pixels;
    if ((x < 0) || (y < 0) || (x >= contentWidth) || (y >= contentHeight))
        return;
    pixels[y * contentWidth + x] = pixel;
}
