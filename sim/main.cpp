/*
 * Dramite
 * Copyright 2023 Wenting Zhang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include <signal.h> 
#include <SDL.h>

#include "config.h"

#include "verilated.h"
#include "verilated_vcd_c.h"

// Verilator related
#include "Vsystem.h"
Vsystem *core;
VerilatedVcdC *trace;
uint64_t tickcount;

// Simulated peripherals
#include "avalonmem.h"
#include "ps2sim.h"
#include "dispsim.h"
#include "cpusim.h"
#include "cpucore.h"

AvalonMem *mainram;
PS2sim *kbdsim;
DispSim *dispsim;
CPUSim *cpusim;
std::thread *cpu_thread;

// Settings
bool enable_trace = false;
bool unlimited = true;
uint64_t trace_from = 0;
uint64_t max_cycles = 100;

static volatile sig_atomic_t running = 1;

// Used by model
double sc_time_stamp() {
    // This is in pS. Currently we use a 10ns (100MHz) clock signal.
    return (double)tickcount * (double)CLK_PREIOD_PS;
}

static void sig_handler(int _) {
    (void)_;
    running = 0;
}

// Hack to allow cpucore to stop the simulation
void stop_simulation() {
    running = 0;
}

void tick() {
    static uint8_t vga_div = 1;
    // Input signals are cached here
    uint32_t readdata;
    uint8_t waitrequest;
    uint8_t readdatavalid;
    uint8_t kbclk_in;
    uint8_t kbdat_in;
    uint32_t cpu_a = core->cpu_a;
    uint32_t cpu_d_in = core->cpu_d_in;
    uint8_t cpu_ads_n = core->cpu_ads_n;
    uint8_t cpu_be_n = core->cpu_be_n;
    uint8_t cpu_dc = core->cpu_dc;
    uint8_t cpu_lock_n = core->cpu_lock_n;
    uint8_t cpu_mio = core->cpu_mio;
    uint8_t cpu_wr = core->cpu_wr;

    mainram->apply(
        core->sdram_address,
        core->sdram_byteenable,
        core->sdram_read,
        readdata,
        core->sdram_write,
        core->sdram_writedata,
        waitrequest,
        readdatavalid,
        core->sdram_burstcount);

    kbdsim->apply(
        core->ps2_kbclk_ena,
        core->ps2_kbclk_out,
        kbclk_in,
        core->ps2_kbdat_ena,
        core->ps2_kbdat_out,
        kbdat_in);

    dispsim->apply(
        core->vga_clock,
        core->vga_horiz_sync,
        core->vga_vert_sync,
        core->vga_r,
        core->vga_g,
        core->vga_b,
        core->vga_blank_n);
    
    cpusim->apply(
        cpu_a,
        core->cpu_d_out,
        cpu_d_in,
        cpu_ads_n,
        cpu_be_n,
        core->cpu_busy_n,
        core->cpu_clk2,
        cpu_dc,
        core->cpu_error_n,
        cpu_lock_n,
        core->cpu_intr,
        cpu_mio,
        core->cpu_nmi,
        core->cpu_pereq,
        core->cpu_ready_n,
        core->cpu_reset,
        cpu_wr);

    // Clock posedge
    core->clk_sys = 1;
    core->clk_vga = vga_div;
    core->eval();

    // Apply changed input signals after clock edge
    core->sdram_readdata = readdata;
    core->sdram_waitrequest = waitrequest;
    core->sdram_readdatavalid = readdatavalid;
    core->ps2_kbclk_in = kbclk_in;
    core->ps2_kbdat_in = kbdat_in;
    core->cpu_a = cpu_a;
    core->cpu_d_in = cpu_d_in;
    core->cpu_ads_n = cpu_ads_n;
    core->cpu_be_n = cpu_be_n;
    core->cpu_dc = cpu_dc;
    core->cpu_lock_n = cpu_lock_n;
    core->cpu_mio = cpu_mio;
    core->cpu_wr = cpu_wr;

    // Let combinational changes propergate, only eval if waveform trace is on
    if (enable_trace && (tickcount >= trace_from)) {
        core->eval();
        trace->dump(tickcount * CLK_PREIOD_PS);
    }

    // Clk negedge
    core->clk_sys = 0;
    core->clk_vga = vga_div;
    core->eval();
    if (enable_trace && (tickcount >= trace_from))
        trace->dump(tickcount * CLK_PREIOD_PS + CLK_HALF_PERIOD_PS);
    
    tickcount++;
    vga_div = !vga_div;
}

void reset() {
    // Stop the thread if currently runnning
    // FIXME: Currently there is no way to tell it to stop
    if (cpu_thread)
        cpu_thread->join();
    cpu_thread = NULL;

    core->reset_sys = 1;
    core->reset_vga = 1;
    for (int i = 0; i < 10; i++) {
        tick();
    }
    core->reset_sys = 0;
    core->reset_vga = 0;
    // RESET SIMULATED DEVICE HERE
    mainram->reset();
    kbdsim->reset();

    cpusim->start();

    g_cpu.reset();
    // Start the thread
    printf("Spawn CPU thread.\n");
    cpu_thread = new std::thread(&CPU::run, &g_cpu);
}

int main(int argc, char *argv[]) {
    printf("Dramite simulator\n");

    // Initialize testbench
    Verilated::commandArgs(argc, argv);

    core = new Vsystem;
    Verilated::traceEverOn(true);

    mainram = new AvalonMem(0x0, RAM_SIZE);
    kbdsim = new PS2sim();
    dispsim = new DispSim();
    cpusim = new CPUSim();

    g_cpu.init(cpusim->m_cpubus);

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "--limit") == 0) {
            if (i == (argc - 1)) {
                fprintf(stderr, "Error: no cycle limit provided\n");
                exit(1);
            }
            else {
                max_cycles = atoll(argv[i + 1]);
                printf("Cycle limit set to %lu.\n", max_cycles);
                unlimited = false;
            }
        }
        else if (strcmp(argv[i], "--trace") == 0) {
            trace_from = 0;
            enable_trace = true;
        }
        else if (strcmp(argv[i], "--trace_from") == 0) {
            trace_from = atoll(argv[i + 1]);
            printf("Trace would only start after %lu cycles.\n", trace_from);
            enable_trace = true;
        }
    }

    if (enable_trace) {
        trace = new VerilatedVcdC;
        core->trace(trace, 99);
        trace->open("trace.vcd");        
    }

    // Load BIOS
    mainram->load("../bios/BIOS-bochs-legacy", 0xf0000);
    mainram->load("../bios/bin/vgabios-lgpl", 0xc0000);

    // Start simulation
    printf("Simulation start.\n");

    clock_t time = clock();

    reset();

    signal(SIGINT, sig_handler);

    while (running) {
        tick();

        if ((!unlimited) && (tickcount > max_cycles)) {
            break;
        }

        if ((tickcount % 4096 == 0)) {
            SDL_Event event;
            if (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    // Break out of the loop on quit
                    running = false;
                }
            }
        }
    }

    printf("Stop.\n");

    time = clock() - time;
    time /= (CLOCKS_PER_SEC / 1000);
    printf("Executed %ld cycles in %ld ms. Average clock speed: %ld kHz\n",
                tickcount, time, tickcount / time);

    if (enable_trace) {
        trace->close();
        delete trace;
    }

    delete core;
    delete mainram;
    delete kbdsim;
    delete dispsim;
    delete cpusim;

    return 0;
}