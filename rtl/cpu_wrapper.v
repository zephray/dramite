/*
 * Copyright (c) 2023, Wenting Zhang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

`default_nettype none

module cpu_wrapper (
    input  wire        clk,
    input  wire        rst,
    input  wire [31:2] cpu_a,
    input  wire [31:0] cpu_d_in,
    output wire [31:0] cpu_d_out,
    output wire        cpu_d_oen,
    input  wire        cpu_ads_n,
    input  wire [ 3:0] cpu_be_n,
    output wire        cpu_busy_n,
    output wire        cpu_clk2,
    input  wire        cpu_dc,
    output wire        cpu_error_n,
    input  wire        cpu_lock_n,
    output wire        cpu_intr,
    input  wire        cpu_mio,
    output wire        cpu_nmi,
    output wire        cpu_pereq,
    output wire        cpu_ready_n,
    output wire        cpu_reset,
    input  wire        cpu_wr,
    input  wire        int_req,
    output wire        int_ack,
    input  wire [ 7:0] int_vec,
    output wire [31:0] avm_address,
    output wire [31:0] avm_writedata,
    output wire [ 3:0] avm_byteenable,
    output wire [ 2:0] avm_burstcount,
    output wire        avm_write,
    output wire        avm_read,
    input  wire        avm_waitrequest,
    input  wire        avm_readdatavalid,
    input  wire [31:0] avm_readdata,
    output wire [15:0] avalon_io_address,
    output wire [ 3:0] avalon_io_byteenable,
    output wire        avalon_io_read,
    input  wire        avalon_io_readdatavalid,
    input  wire [31:0] avalon_io_readdata,
    output wire        avalon_io_write,
    output wire [31:0] avalon_io_writedata,
    input  wire        avalon_io_waitrequest,
    output wire        halt,
    output wire        shutdown
);

`ifdef USE_AO486_CPU
    // Use ao486 CPU
    ao486 ao486 (
        .clk(clk),
        .rst(rst),
        .interrupt_do(int_req),
        .interrupt_vector(int_vec),
        .interrupt_done(int_ack),
        .avm_address(avm_address),
        .avm_writedata(avm_writedata),
        .avm_byteenable(avm_byteenable),
        .avm_burstcount(avm_burstcount),
        .avm_write(avm_write),
        .avm_read(avm_read),
        .avm_waitrequest(avm_waitrequest),
        .avm_readdatavalid(avm_readdatavalid),
        .avm_readdata(avm_readdata),
        .avalon_io_address(avalon_io_address),
        .avalon_io_byteenable(avalon_io_byteenable),
        .avalon_io_read(avalon_io_read),
        .avalon_io_readdatavalid(avalon_io_readdatavalid),
        .avalon_io_readdata(avalon_io_readdata),
        .avalon_io_write(avalon_io_write),
        .avalon_io_writedata(avalon_io_writedata),
        .avalon_io_waitrequest(avalon_io_waitrequest)
    );

    assign cpu_d_out = 32'b0;
    assign cpu_d_oen = 1'b0;
    assign cpu_busy_n = 1'b0;
    assign cpu_clk2 = 1'b0;
    assign cpu_error_n = 1'b0;
    assign cpu_intr = 1'b0;
    assign cpu_nmi = 1'b0;
    assign cpu_pereq = 1'b0;
    assign cpu_ready_n = 1'b0;
    assign cpu_reset = 1'b0;
`else
    // Use real external CPU
    // If cache is used, wrap it here as well
    cpu_interface cpu_interface (
        .clk(clk),
        .rst(rst),
        .cpu_a(cpu_a),
        .cpu_d_in(cpu_d_in),
        .cpu_d_out(cpu_d_out),
        .cpu_d_oen(cpu_d_oen),
        .cpu_ads_n(cpu_ads_n),
        .cpu_be_n(cpu_be_n),
        .cpu_busy_n(cpu_busy_n),
        .cpu_clk2(cpu_clk2),
        .cpu_dc(cpu_dc),
        .cpu_error_n(cpu_error_n),
        .cpu_lock_n(cpu_lock_n),
        .cpu_intr(cpu_intr),
        .cpu_mio(cpu_mio),
        .cpu_nmi(cpu_nmi),
        .cpu_pereq(cpu_pereq),
        .cpu_ready_n(cpu_ready_n),
        .cpu_reset(cpu_reset),
        .cpu_wr(cpu_wr),
        .avm_address(avm_address),
        .avm_byteenable(avm_byteenable),
        .avm_burstcount(avm_burstcount),
        .avm_read(avm_read),
        .avm_readdatavalid(avm_readdatavalid),
        .avm_readdata(avm_readdata),
        .avm_write(avm_write),
        .avm_writedata(avm_writedata),
        .avm_waitrequest(avm_waitrequest),
        .avalon_io_address(avalon_io_address),
        .avalon_io_byteenable(avalon_io_byteenable),
        .avalon_io_read(avalon_io_read),
        .avalon_io_readdatavalid(avalon_io_readdatavalid),
        .avalon_io_readdata(avalon_io_readdata),
        .avalon_io_write(avalon_io_write),
        .avalon_io_writedata(avalon_io_writedata),
        .avalon_io_waitrequest(avalon_io_waitrequest),
        .int_req(int_req),
        .int_ack(int_ack),
        .int_vec(int_vec),
        .halt(halt),
        .shutdown(shutdown)
    );
`endif

endmodule
