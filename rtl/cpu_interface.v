/*
 * Copyright (c) 2023, Wenting Zhang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

`default_nettype none

module cpu_interface (
    // Clock & Reset
    input wire clk,  // Base Clock Input (2xCPU clock)
    input wire rst,  // Clean Active High reset input

    // CPU interface
    input  wire [31:2] cpu_a,
    input  wire [31:0] cpu_d_in,
    output wire [31:0] cpu_d_out,
    output wire        cpu_d_oen,
    input  wire        cpu_ads_n,    // Address Status
    input  wire [ 3:0] cpu_be_n,
    output wire        cpu_busy_n,   // Busy
    output wire        cpu_clk2,     // Clock
    input  wire        cpu_dc,       // H: Data, Low: Control
    output wire        cpu_error_n,  // Error
    /* verilator lint_off UNUSED */
    // There is nothing else on the CPU bus, no need to hold the bus, for now.
    input  wire        cpu_lock_n,   // Bus Lock
    /* verilator lint_on UNUSED */
    output wire        cpu_intr,     // Interrupt Request
    input  wire        cpu_mio,      // H: Memory, L: IO
    output wire        cpu_nmi,      // Non-Maskable Interrupt Request
    output wire        cpu_pereq,    // Processor Extension Request
    output reg         cpu_ready_n,  // Bus Ready
    output wire        cpu_reset,    // Reset
    input  wire        cpu_wr,       // H: Write, L: Read

    // Avalon Memory Port
    output reg  [31:0] avm_address,
    output reg  [ 3:0] avm_byteenable,
    output reg  [ 2:0] avm_burstcount,
    output reg         avm_read,
    input  wire        avm_readdatavalid,
    input  wire [31:0] avm_readdata,
    output reg         avm_write,
    output reg  [31:0] avm_writedata,
    input  wire        avm_waitrequest,

    // Avalon IO Port
    output reg  [15:0] avalon_io_address,
    output reg  [ 3:0] avalon_io_byteenable,
    output reg         avalon_io_read,
    input  wire        avalon_io_readdatavalid,
    input  wire [31:0] avalon_io_readdata,
    output reg         avalon_io_write,
    output reg  [31:0] avalon_io_writedata,
    input  wire        avalon_io_waitrequest,

    // Interrupt controller interface
    input  wire       int_req,
    output reg        int_ack,
    input  wire [7:0] int_vec,

    // Special state
    output reg        halt,
    output reg        shutdown
);

    // Clock & Reset
    assign cpu_clk2 = clk;
    assign cpu_reset = rst;

    // Coprocessor interface is not supported for now
    assign cpu_pereq = 1'b0;
    assign cpu_busy_n = 1'b1;
    assign cpu_error_n = 1'b1;

    // Unhandled for now
    assign cpu_nmi = 1'b0;
    assign cpu_intr = int_req;

    // Doesn't generate burst
    assign avm_burstcount = 3'd1;

    // 386 Bus
    localparam CPU_BUS_S0 = 3'd0;  // Idle, no bus req       000
    localparam CPU_BUS_S1 = 3'd1;  // Start Bus Transfer     001
    localparam CPU_BUS_S2 = 3'd2;  // Check R/W Status       010
    localparam CPU_BUS_S3 = 3'd3;  // Waiting for Bus finish 011
    localparam CPU_BUS_S4 = 3'd4;  // Waiting for R/W finish 100
    // Only handle non-pipelined transfer
    // Note: D/C is not currently handled, lock is not used now.
    reg [2:0] cpu_bus_state;

    reg [31:0] cpu_d_wr;
    reg cpu_d_dir;  // 0: Ext->Int, 1: Int->Ext
    assign cpu_d_out = cpu_d_wr;
    assign cpu_d_oen = cpu_d_dir;

    reg downlink_finished;
    reg [31:0] downlink_data;

    // Data
    wire cycle_type_mem = cpu_mio && !(!cpu_dc && cpu_wr);
    wire cycle_type_io = !cpu_mio && cpu_dc;

    // Control
    wire cycle_type_intack = !cpu_mio && !cpu_dc && !cpu_wr;
    wire cycle_type_haltshut = cpu_mio && !cpu_dc && cpu_wr;
    //wire cycle_type_halt = ccycle_type_haltshut && (cpu_be_n == 4'b1011);
    //wire cycle_type_shutdown = ccycle_type_haltshut && (cpu_be_n == 4'b1110);

    always @(posedge cpu_clk2, posedge rst) begin
        case (cpu_bus_state)
            CPU_BUS_S0: begin
                cpu_d_dir <= 1'b0;
                cpu_ready_n <= 1'b1;  // Data not ready
                if (!cpu_ads_n) begin
                    // Bus cycle start
                    if (cycle_type_io) begin
                        // IO R/W, send to bus port
                        avalon_io_address <= {cpu_a, 2'b0};
                        avalon_io_read <= !cpu_wr;
                        avalon_io_write <= 1'b0;
                        avalon_io_byteenable <= ~cpu_be_n;
                    end else if (cycle_type_mem) begin
                        avm_address <= {cpu_a, 2'b0};
                        avm_read <= !cpu_wr;
                        avm_write <= 1'b0;
                        avm_byteenable <= ~cpu_be_n;
                    end
                    cpu_bus_state <= CPU_BUS_S1;
                    downlink_finished <= 1'b0;
                end
            end
            CPU_BUS_S1: begin
                // At this cycle the memory sees the request
                if (cycle_type_io && cpu_wr) begin
                    avalon_io_write <= 1'b1;
                    avalon_io_writedata <= cpu_d_in;
                end else if (cycle_type_mem && cpu_wr) begin
                    avm_write <= 1'b1;
                    avm_writedata <= cpu_d_in;
                end else if (avalon_io_read && avalon_io_readdatavalid) begin
                    downlink_finished <= 1'b1;
                    downlink_data <= avalon_io_readdata;
                    avalon_io_read <= 1'b0;
                end else if (avm_read && avm_readdatavalid) begin
                    downlink_finished <= 1'b1;
                    downlink_data <= avm_readdata;
                    avm_read <= 1'b0;
                end
                cpu_bus_state <= CPU_BUS_S2;
            end
            CPU_BUS_S2: begin
                if (cpu_wr) begin
                    // Check if write have finished
                    if (downlink_finished) begin
                        cpu_ready_n <= 1'b0;
                        cpu_bus_state <= CPU_BUS_S3;
                    end else if (cycle_type_io) begin
                        if (!avalon_io_waitrequest) begin
                            avalon_io_write <= 1'b0;
                            cpu_ready_n <= 1'b0;
                            cpu_bus_state <= CPU_BUS_S3;
                        end else begin
                            cpu_bus_state <= CPU_BUS_S4;
                        end
                    end else if (cycle_type_mem) begin
                        if (!avm_waitrequest) begin
                            avm_write <= 1'b0;
                            cpu_ready_n <= 1'b0;
                            cpu_bus_state <= CPU_BUS_S3;
                        end else begin
                            cpu_bus_state <= CPU_BUS_S4;
                        end
                    end else if (cycle_type_haltshut) begin
                        if (cpu_be_n[0]) begin
                            shutdown <= 1'b1;
                        end else begin
                            halt <= 1'b1;
                        end
                        cpu_ready_n <= 1'b0;
                        cpu_bus_state <= CPU_BUS_S3;
                        // TODO: Exit halt/shutdown state
                    end
                end else begin
                    // Check if read have finished 
                    if (downlink_finished) begin
                        cpu_d_dir <= 1'b1;
                        cpu_d_wr <= downlink_data;
                        cpu_ready_n <= 1'b0;
                        cpu_bus_state <= CPU_BUS_S3;
                    end else if (cycle_type_io) begin
                        if (avalon_io_readdatavalid) begin
                            avalon_io_read <= 1'b0;
                            cpu_d_dir <= 1'b1;
                            cpu_d_wr <= avalon_io_readdata;
                            cpu_ready_n <= 1'b0;
                            cpu_bus_state <= CPU_BUS_S3;
                        end else begin
                            cpu_bus_state <= CPU_BUS_S4;
                        end
                    end else if (cycle_type_mem) begin
                        if (avm_readdatavalid) begin
                            avm_read <= 1'b0;
                            cpu_d_dir <= 1'b1;
                            cpu_d_wr <= avm_readdata;
                            cpu_ready_n <= 1'b0;
                            cpu_bus_state <= CPU_BUS_S3;
                        end else begin
                            cpu_bus_state <= CPU_BUS_S4;
                        end
                    end else if (cycle_type_intack) begin
                        if (cpu_a[2]) begin
                            int_ack <= 1'b1;
                        end else begin
                            int_ack <= 1'b0;
                            cpu_d_dir <= 1'b1;
                            cpu_d_wr <= {24'b0, int_vec};
                        end
                        cpu_ready_n <= 1'b0;
                        cpu_bus_state <= CPU_BUS_S3;
                    end
                end
            end
            CPU_BUS_S3: begin
                cpu_bus_state <= CPU_BUS_S0;
            end
            CPU_BUS_S4: begin
                // Check slave finish
                if (avalon_io_read && avalon_io_readdatavalid) begin
                    downlink_finished <= 1'b1;
                    downlink_data <= avalon_io_readdata;
                    avalon_io_read <= 1'b0;
                end else if (avm_read && avm_readdatavalid) begin
                    downlink_finished <= 1'b1;
                    downlink_data <= avm_readdata;
                    avm_read <= 1'b0;
                end else if (avalon_io_write && !avalon_io_waitrequest) begin
                    downlink_finished <= 1'b1;
                    avalon_io_write <= 1'b0;
                end else if (avm_write && !avm_waitrequest) begin
                    downlink_finished <= 1'b1;
                    avm_write <= 1'b0;
                end
                cpu_bus_state <= CPU_BUS_S2;
            end
            default: begin
                cpu_bus_state <= CPU_BUS_S0;
            end
        endcase
        if (rst) begin
            cpu_bus_state <= CPU_BUS_S0;
            cpu_ready_n <= 1'b1;
            cpu_d_dir <= 1'b0;
            shutdown <= 1'b0;
            halt <= 1'b0;
            avm_write <= 1'b0;
            avm_read <= 1'b0;
        end
    end

endmodule
