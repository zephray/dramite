wire cond_0;
wire cond_1;
wire cond_2;
wire cond_3;
wire cond_4;
wire after_invalidate_to_reg;
wire [7:0] invd_counter_to_reg ;
wire state_to_reg;
wire init_done_to_reg;

//======================================================== conditions
 assign cond_0 = init_done == `FALSE;
 assign cond_1 = invd_counter == 8'd255;
 assign cond_2 = state == STATE_IDLE;
 assign cond_3 = init_done && invdcode_do;
 assign cond_4 = state == STATE_INVD;
//======================================================== saves
 assign after_invalidate_to_reg =
    (cond_0 && cond_1)? ( `TRUE) :
    (cond_2)? ( `FALSE) :
    (cond_4 && cond_1)? ( `TRUE) :
    after_invalidate;
 assign invd_counter_to_reg =
    (cond_0)? ( invd_counter + 8'd1) :
    (cond_4)? ( invd_counter + 8'd1) :
    invd_counter;
 assign state_to_reg =
    (cond_2 && cond_3)? ( STATE_INVD) :
    (cond_4 && cond_1)? ( STATE_IDLE) :
    state;
 assign init_done_to_reg =
    (cond_0 && cond_1)? ( `TRUE) :
    init_done;
//======================================================== always
always @(posedge clk) begin
    if(rst) after_invalidate <= 1'd0;
    else              after_invalidate <= after_invalidate_to_reg;
end
always @(posedge clk) begin
    if(rst) invd_counter <= 8'd0;
    else              invd_counter <= invd_counter_to_reg;
end
always @(posedge clk) begin
    if(rst) state <= 1'd0;
    else              state <= state_to_reg;
end
always @(posedge clk) begin
    if(rst) init_done <= 1'd0;
    else              init_done <= init_done_to_reg;
end
//======================================================== sets
assign invdcode_done =
    (cond_4 && cond_1)? (`TRUE) :
    1'd0;
