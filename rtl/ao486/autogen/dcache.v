wire cond_0;
wire cond_1;
wire cond_2;
wire cond_3;
wire cond_4;
wire cond_5;
wire cond_6;
wire cond_7;
wire cond_8;
wire cond_9;
wire cond_10;
wire cond_11;
wire cond_12;
wire cond_13;
wire cond_14;
wire cond_15;
wire cond_16;
wire cond_17;
wire cond_18;
wire cond_19;
wire cond_20;
wire cond_21;
wire cond_22;
wire cond_23;
wire cond_24;
wire cond_25;
wire cond_26;
wire cond_27;
wire is_write_to_reg;
wire [31:0] address_to_reg ;
wire [2:0] state_to_reg ;
wire [3:0] length_to_reg ;
wire write_through_to_reg;
wire [31:0] write_data_to_reg ;
wire cache_disable_to_reg;

//======================================================== conditions
 assign cond_0 = state == STATE_IDLE;
 assign cond_1 = invddata_do;
 assign cond_2 = wbinvddata_do;
 assign cond_3 = dcachewrite_do;
 assign cond_4 = dcacheread_do;
 assign cond_5 = state == STATE_READ_CHECK;
 assign cond_6 = matched;
 assign cond_7 = cache_disable;
 assign cond_8 = writeback_needed;
 assign cond_9 = state == STATE_WRITE_CHECK;
 assign cond_10 = matched_index == 2'd0;
 assign cond_11 = matched_index == 2'd1;
 assign cond_12 = matched_index == 2'd2;
 assign cond_13 = matched_index == 2'd3;
 assign cond_14 = write_through;
 assign cond_15 = state == STATE_READ_BURST;
 assign cond_16 = readburst_done;
 assign cond_17 = state == STATE_WRITE_BACK;
 assign cond_18 = writeline_done;
 assign cond_19 = state == STATE_READ_LINE;
 assign cond_20 = readline_done;
 assign cond_21 = is_write;
 assign cond_22 = plru_index == 2'd0;
 assign cond_23 = plru_index == 2'd1;
 assign cond_24 = plru_index == 2'd2;
 assign cond_25 = plru_index == 2'd3;
 assign cond_26 = state == STATE_WRITE_THROUGH;
 assign cond_27 = writeburst_done;
//======================================================== saves
 assign is_write_to_reg =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? ( `TRUE) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? ( `FALSE) :
    is_write;
 assign address_to_reg =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (        dcachewrite_address) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? (        dcacheread_address) :
    address;
 assign state_to_reg =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? ( STATE_WRITE_CHECK) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? ( STATE_READ_CHECK) :
    (cond_5 && cond_6)? ( STATE_IDLE) :
    (cond_5 && ~cond_6 && cond_7)? ( STATE_READ_BURST) :
    (cond_5 && ~cond_6 && ~cond_7 && cond_8)? ( STATE_WRITE_BACK) :
    (cond_5 && ~cond_6 && ~cond_7 && ~cond_8)? ( STATE_READ_LINE) :
    (cond_9 && cond_6 && cond_14)? ( STATE_WRITE_THROUGH) :
    (cond_9 && cond_6 && ~cond_14)? ( STATE_IDLE) :
    (cond_9 && ~cond_6 && cond_7)? ( STATE_WRITE_THROUGH) :
    (cond_9 && ~cond_6 && ~cond_7 && cond_8)? ( STATE_WRITE_BACK) :
    (cond_9 && ~cond_6 && ~cond_7 && ~cond_8)? ( STATE_READ_LINE) :
    (cond_15 && cond_16)? ( STATE_IDLE) :
    (cond_17 && cond_18)? ( STATE_READ_LINE) :
    (cond_19 && cond_20 && cond_21 && cond_14)? ( STATE_WRITE_THROUGH) :
    (cond_19 && cond_20 && cond_21 && ~cond_14)? ( STATE_IDLE) :
    (cond_19 && cond_20 && ~cond_21)? ( STATE_IDLE) :
    (cond_26 && cond_27)? ( STATE_IDLE) :
    state;
 assign length_to_reg =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (         { 1'b0, dcachewrite_length }) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? (         dcacheread_length) :
    length;
 assign write_through_to_reg =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (  dcachewrite_write_through) :
    write_through;
 assign write_data_to_reg =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (     dcachewrite_data) :
    write_data;
 assign cache_disable_to_reg =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (  dcachewrite_cache_disable) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? (  dcacheread_cache_disable) :
    cache_disable;
//======================================================== always
always @(posedge clk) begin
    if(rst) is_write <= 1'd0;
    else              is_write <= is_write_to_reg;
end
always @(posedge clk) begin
    if(rst) address <= 32'd0;
    else              address <= address_to_reg;
end
always @(posedge clk) begin
    if(rst) state <= 3'd0;
    else              state <= state_to_reg;
end
always @(posedge clk) begin
    if(rst) length <= 4'd0;
    else              length <= length_to_reg;
end
always @(posedge clk) begin
    if(rst) write_through <= 1'd0;
    else              write_through <= write_through_to_reg;
end
always @(posedge clk) begin
    if(rst) write_data <= 32'd0;
    else              write_data <= write_data_to_reg;
end
always @(posedge clk) begin
    if(rst) cache_disable <= 1'd0;
    else              cache_disable <= cache_disable_to_reg;
end
//======================================================== sets
assign control_ram_read_do =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (`TRUE) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? (`TRUE) :
    1'd0;
assign data_ram3_write_do =
    (cond_9 && cond_6 && cond_13)? (`TRUE) :
    (cond_19 && cond_20 && cond_21 && cond_25)? (`TRUE) :
    (cond_19 && cond_20 && ~cond_21 && cond_25)? (`TRUE) :
    1'd0;
assign writeburst_byteenable_0 =
    (cond_9 && cond_6 && cond_14)? ( write_burst_byteenable_0) :
    (cond_9 && ~cond_6 && cond_7)? ( write_burst_byteenable_0) :
    (cond_19 && cond_20 && cond_21 && cond_14)? ( write_burst_byteenable_0) :
    4'd0;
assign writeburst_byteenable_1 =
    (cond_9 && cond_6 && cond_14)? ( write_burst_byteenable_1) :
    (cond_9 && ~cond_6 && cond_7)? ( write_burst_byteenable_1) :
    (cond_19 && cond_20 && cond_21 && cond_14)? ( write_burst_byteenable_1) :
    4'd0;
assign dcachewrite_done =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (`TRUE) :
    1'd0;
assign data_ram0_address =
    (cond_0 && ~cond_1 && cond_2)? ( { 20'd0, wbinvdread_address, 4'd0 }) :
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? ( dcachewrite_address) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? ( dcacheread_address) :
    (cond_9 && cond_6 && cond_10)? (   address) :
    (cond_19 && cond_20 && cond_21 && cond_22)? (   address) :
    (cond_19 && cond_20 && ~cond_21 && cond_22)? (   address) :
    32'd0;
assign data_ram0_data =
    (cond_9 && cond_6 && cond_10)? (      line_merged) :
    (cond_19 && cond_20 && cond_21 && cond_22)? (      line_merged) :
    (cond_19 && cond_20 && ~cond_21 && cond_22)? (      readline_line) :
    128'd0;
assign data_ram1_write_do =
    (cond_9 && cond_6 && cond_11)? (`TRUE) :
    (cond_19 && cond_20 && cond_21 && cond_23)? (`TRUE) :
    (cond_19 && cond_20 && ~cond_21 && cond_23)? (`TRUE) :
    1'd0;
assign readburst_byte_length =
    (cond_5 && ~cond_6 && cond_7)? (  read_burst_byte_length) :
    4'd0;
assign data_ram0_read_do =
    (cond_0 && ~cond_1 && cond_2)? ( wbinvdread_do) :
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (`TRUE) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? (`TRUE) :
    1'd0;
assign data_ram2_data =
    (cond_9 && cond_6 && cond_12)? (      line_merged) :
    (cond_19 && cond_20 && cond_21 && cond_24)? (      line_merged) :
    (cond_19 && cond_20 && ~cond_21 && cond_24)? (      readline_line) :
    128'd0;
assign dcache_writeline_line =
    (cond_5 && ~cond_6 && ~cond_7 && cond_8)? (    plru_data_line[127:0]) :
    (cond_9 && ~cond_6 && ~cond_7 && cond_8)? (    plru_data_line[127:0]) :
    128'd0;
assign readline_do =
    (cond_5 && ~cond_6 && ~cond_7 && ~cond_8)? (`TRUE) :
    (cond_9 && ~cond_6 && ~cond_7 && ~cond_8)? (`TRUE) :
    (cond_17 && cond_18)? (`TRUE) :
    1'd0;
assign readline_address =
    (cond_5 && ~cond_6 && ~cond_7 && ~cond_8)? ( { address[31:4], 4'd0 }) :
    (cond_9 && ~cond_6 && ~cond_7 && ~cond_8)? ( { address[31:4], 4'd0 }) :
    (cond_17 && cond_18)? ( { address[31:4], 4'd0 }) :
    32'd0;
assign dcachetoicache_write_do =
    (cond_9 && cond_6 && ~cond_14)? (`TRUE) :
    (cond_19 && cond_20 && cond_21 && ~cond_14)? (`TRUE) :
    (cond_26 && cond_27)? (`TRUE) :
    1'd0;
assign writeburst_data =
    (cond_9 && cond_6 && cond_14)? (         write_burst_data) :
    (cond_9 && ~cond_6 && cond_7)? (         write_burst_data) :
    (cond_19 && cond_20 && cond_21 && cond_14)? (         write_burst_data) :
    56'd0;
assign readburst_address =
    (cond_5 && ~cond_6 && cond_7)? (      address) :
    32'd0;
assign readburst_dword_length =
    (cond_5 && ~cond_6 && cond_7)? ( read_burst_dword_length) :
    2'd0;
assign dcachetoicache_write_address =
    (cond_9 && cond_6 && ~cond_14)? ( address) :
    (cond_19 && cond_20 && cond_21 && ~cond_14)? ( address) :
    (cond_26 && cond_27)? ( address) :
    32'd0;
assign data_ram0_write_do =
    (cond_9 && cond_6 && cond_10)? (`TRUE) :
    (cond_19 && cond_20 && cond_21 && cond_22)? (`TRUE) :
    (cond_19 && cond_20 && ~cond_21 && cond_22)? (`TRUE) :
    1'd0;
assign dcache_writeline_address =
    (cond_5 && ~cond_6 && ~cond_7 && cond_8)? ( { plru_data_line[147:128], address[11:4], 4'd0 }) :
    (cond_9 && ~cond_6 && ~cond_7 && cond_8)? ( { plru_data_line[147:128], address[11:4], 4'd0 }) :
    32'd0;
assign data_ram2_write_do =
    (cond_9 && cond_6 && cond_12)? (`TRUE) :
    (cond_19 && cond_20 && cond_21 && cond_24)? (`TRUE) :
    (cond_19 && cond_20 && ~cond_21 && cond_24)? (`TRUE) :
    1'd0;
assign data_ram1_read_do =
    (cond_0 && ~cond_1 && cond_2)? ( wbinvdread_do) :
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (`TRUE) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? (`TRUE) :
    1'd0;
assign data_ram3_data =
    (cond_9 && cond_6 && cond_13)? (      line_merged) :
    (cond_19 && cond_20 && cond_21 && cond_25)? (      line_merged) :
    (cond_19 && cond_20 && ~cond_21 && cond_25)? (      readline_line) :
    128'd0;
assign data_ram3_read_do =
    (cond_0 && ~cond_1 && cond_2)? ( wbinvdread_do) :
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (`TRUE) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? (`TRUE) :
    1'd0;
assign data_ram3_address =
    (cond_0 && ~cond_1 && cond_2)? ( { 20'd0, wbinvdread_address, 4'd0 }) :
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? ( dcachewrite_address) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? ( dcacheread_address) :
    (cond_9 && cond_6 && cond_13)? (   address) :
    (cond_19 && cond_20 && cond_21 && cond_25)? (   address) :
    (cond_19 && cond_20 && ~cond_21 && cond_25)? (   address) :
    32'd0;
assign writeburst_do =
    (cond_9 && cond_6 && cond_14)? (`TRUE) :
    (cond_9 && ~cond_6 && cond_7)? (`TRUE) :
    (cond_19 && cond_20 && cond_21 && cond_14)? (`TRUE) :
    1'd0;
assign data_ram1_data =
    (cond_9 && cond_6 && cond_11)? (      line_merged) :
    (cond_19 && cond_20 && cond_21 && cond_23)? (      line_merged) :
    (cond_19 && cond_20 && ~cond_21 && cond_23)? ( readline_line) :
    128'd0;
assign dcache_writeline_do =
    (cond_5 && ~cond_6 && ~cond_7 && cond_8)? (`TRUE) :
    (cond_9 && ~cond_6 && ~cond_7 && cond_8)? (`TRUE) :
    1'd0;
assign dcacheread_done =
    (cond_5 && cond_6)? (`TRUE) :
    (cond_15 && cond_16)? (`TRUE) :
    (cond_19 && cond_20 && ~cond_21)? (`TRUE) :
    1'd0;
assign control_ram_write_do =
    (cond_5 && cond_6)? (`TRUE) :
    (cond_9 && cond_6)? (`TRUE) :
    (cond_19 && cond_20 && cond_21)? (`TRUE) :
    (cond_19 && cond_20 && ~cond_21)? (`TRUE) :
    1'd0;
assign control_ram_data =
    (cond_5 && cond_6)? (        control_after_match) :
    (cond_9 && cond_6)? (        control_after_write_to_existing) :
    (cond_19 && cond_20 && cond_21)? (        control_after_write_to_new) :
    (cond_19 && cond_20 && ~cond_21)? (        control_after_line_read) :
    11'd0;
assign writeburst_dword_length =
    (cond_9 && cond_6 && cond_14)? ( write_burst_dword_length) :
    (cond_9 && ~cond_6 && cond_7)? ( write_burst_dword_length) :
    (cond_19 && cond_20 && cond_21 && cond_14)? ( write_burst_dword_length) :
    2'd0;
assign data_ram2_address =
    (cond_0 && ~cond_1 && cond_2)? ( { 20'd0, wbinvdread_address, 4'd0 }) :
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? ( dcachewrite_address) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? ( dcacheread_address) :
    (cond_9 && cond_6 && cond_12)? (   address) :
    (cond_19 && cond_20 && cond_21 && cond_24)? (   address) :
    (cond_19 && cond_20 && ~cond_21 && cond_24)? (   address) :
    32'd0;
assign data_ram1_address =
    (cond_0 && ~cond_1 && cond_2)? ( { 20'd0, wbinvdread_address, 4'd0 }) :
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? ( dcachewrite_address) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? ( dcacheread_address) :
    (cond_9 && cond_6 && cond_11)? (   address) :
    (cond_19 && cond_20 && cond_21 && cond_23)? (   address) :
    (cond_19 && cond_20 && ~cond_21 && cond_23)? (    address) :
    32'd0;
assign data_ram2_read_do =
    (cond_0 && ~cond_1 && cond_2)? ( wbinvdread_do) :
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? (`TRUE) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? (`TRUE) :
    1'd0;
assign writeburst_address =
    (cond_9 && cond_6 && cond_14)? (      address) :
    (cond_9 && ~cond_6 && cond_7)? (      address) :
    (cond_19 && cond_20 && cond_21 && cond_14)? (      address) :
    32'd0;
assign readburst_do =
    (cond_5 && ~cond_6 && cond_7)? (`TRUE) :
    1'd0;
assign control_ram_address =
    (cond_0 && ~cond_1 && ~cond_2 && cond_3)? ( dcachewrite_address) :
    (cond_0 && ~cond_1 && ~cond_2 && ~cond_3 && cond_4)? (     dcacheread_address) :
    (cond_5 && cond_6)? (     address) :
    (cond_9 && cond_6)? (     address) :
    (cond_19 && cond_20 && cond_21)? (     address) :
    (cond_19 && cond_20 && ~cond_21)? (     address) :
    32'd0;
assign dcacheread_data =
    (cond_5 && cond_6)? ( read_from_line) :
    (cond_15 && cond_16)? ( read_from_burst) :
    (cond_19 && cond_20 && ~cond_21)? ( read_from_line) :
    64'd0;
