/*
 * Copyright (c) 2014, Aleksander Osman
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

`include "defines.v"

//PARSED_COMMENTS: this file contains parsed script comments

module execute_commands(
    input               clk,
    input               rst,
    
    input               exe_reset,
    
    //general input
    input       [31:0]  eax,
    input       [31:0]  ecx,
    input       [31:0]  edx,
    input       [31:0]  ebp,
    input       [31:0]  esp,
    
    input       [31:0]  tr_base,
    
    input       [15:0]  es,
    input       [15:0]  cs,
    input       [15:0]  ss,
    input       [15:0]  ds,
    input       [15:0]  fs,
    input       [15:0]  gs,
    input       [15:0]  ldtr,
    input       [15:0]  tr,
    
    input       [31:0]  cr2,
    input       [31:0]  cr3,
    
    input       [31:0]  dr0,
    input       [31:0]  dr1,
    input       [31:0]  dr2,
    input       [31:0]  dr3,
    input               dr6_bt,
    input               dr6_bs,
    input               dr6_bd,
    input               dr6_b12,
    input       [3:0]   dr6_breakpoints,
    input       [31:0]  dr7,
    
    input       [1:0]   cpl,
    
    input               real_mode,
    input               v8086_mode,
    input               protected_mode,
    
    input               idflag,
    input               acflag,
    input               vmflag,
    input               rflag,
    input               ntflag,
    input       [1:0]   iopl,
    input               oflag,
    input               dflag,
    input               iflag,
    input               tflag,
    input               sflag,
    input               zflag,
    input               aflag,
    input               pflag,
    input               cflag,
    
    input               cr0_pg,
    input               cr0_cd,
    input               cr0_nw,
    input               cr0_am,
    input               cr0_wp,
    input               cr0_ne,
    input               cr0_ts,
    input               cr0_em,
    input               cr0_mp,
    input               cr0_pe,
    
    input       [31:0]  cs_limit,
    input       [31:0]  tr_limit,
    input       [63:0]  tr_cache,
    input       [63:0]  ss_cache,
    
    input       [15:0]  idtr_limit,
    input       [31:0]  idtr_base,
    
    input       [15:0]  gdtr_limit,
    input       [31:0]  gdtr_base,
    
    //exception input
    input               exc_push_error,
    input       [15:0]  exc_error_code,
    input               exc_soft_int_ib,
    input               exc_soft_int,
    input       [7:0]   exc_vector,
    
    //exe input
    input       [10:0]  exe_mutex_current,
    
    input       [31:0]  exe_eip,
    input       [31:0]  exe_extra,
    input       [31:0]  exe_linear,
    input       [6:0]   exe_cmd,
    input       [3:0]   exe_cmdex,
    input       [39:0]  exe_decoder,
    input       [2:0]   exe_modregrm_reg,
    input       [31:0]  exe_address_effective,
    input               exe_is_8bit,
    input               exe_operand_16bit,
    input               exe_operand_32bit,
    input               exe_address_16bit,
    input       [3:0]   exe_consumed,
    
    input       [31:0]  src,
    input       [31:0]  dst,
    
    input       [31:0]  exe_enter_offset,
    
    input               exe_ready,
    
    //mult
    input               mult_busy,
    input       [65:0]  mult_result,
    
    //div
    input               div_busy,
    input               exe_div_exception,
    
    input       [31:0]  div_result_quotient,
    input       [31:0]  div_result_remainder,
    
    //shift
    input               e_shift_no_write,
    input               e_shift_oszapc_update,
    input               e_shift_cf_of_update,
    input               e_shift_oflag,
    input               e_shift_cflag,
    
    input       [31:0]  e_shift_result,
    
    //tlbcheck
    output              tlbcheck_do,
    input               tlbcheck_done,
    input               tlbcheck_page_fault,
    
    output      [31:0]  tlbcheck_address,
    output              tlbcheck_rw,
    //----
    
    //tlbflushsingle
    output              tlbflushsingle_do,
    input               tlbflushsingle_done,
    
    output      [31:0]  tlbflushsingle_address,
    //----
    
    //invd
    output              invdcode_do,
    input               invdcode_done,
    
    output              invddata_do,
    input               invddata_done,
    
    output              wbinvddata_do,
    input               wbinvddata_done,
    //---
    
    //pipeline input
    input       [1:0]   wr_task_rpl,
    input       [31:0]  wr_esp_prev,
    
    //global input
    input       [63:0]  glob_descriptor,
    input       [63:0]  glob_descriptor_2,
    input       [31:0]  glob_param_1,
    input       [31:0]  glob_param_2,
    input       [31:0]  glob_param_3,
    input       [31:0]  glob_param_5,
    
    input       [31:0]  glob_desc_base,
    
    input       [31:0]  glob_desc_limit,
    input       [31:0]  glob_desc_2_limit,
    
    //global set
    output              exe_glob_descriptor_set,
    output      [63:0]  exe_glob_descriptor_value,
    
    output              exe_glob_descriptor_2_set,
    output      [63:0]  exe_glob_descriptor_2_value,
    
    output              exe_glob_param_1_set,
    output      [31:0]  exe_glob_param_1_value,
    
    output              exe_glob_param_2_set,
    output      [31:0]  exe_glob_param_2_value,
    
    output              exe_glob_param_3_set,
    output      [31:0]  exe_glob_param_3_value,
    
    output              dr6_bd_set,
    
    //offset control
    output              offset_ret_far_se,
    output              offset_new_stack,
    output              offset_new_stack_minus,
    output              offset_new_stack_continue,
    output              offset_leave,
    output              offset_pop,
    output              offset_enter_last,
    output              offset_ret,
    output              offset_iret_glob_param_4,
    output              offset_iret,
    output              offset_ret_imm,
    output              offset_esp,
    output              offset_call,
    output              offset_call_keep,
    output              offset_call_int_same_first,
    output              offset_call_int_same_next,
    output              offset_int_real,
    output              offset_int_real_next,
    output              offset_task,
    
    //task output
    output      [31:0]  task_eip,

    //exe output
    output              exe_waiting,
    
    output              exe_bound_fault,
    output              exe_trigger_gp_fault,
    output              exe_trigger_ts_fault,
    output              exe_trigger_ss_fault,
    output              exe_trigger_np_fault,
    output              exe_trigger_pf_fault,
    output              exe_trigger_db_fault,
    output              exe_trigger_nm_fault,
    output              exe_load_seg_gp_fault,
    output              exe_load_seg_ss_fault,
    output              exe_load_seg_np_fault,
    
    output      [15:0]  exe_error_code,
    
    output      [31:0]  exe_result,
    output      [31:0]  exe_result2,
    output      [31:0]  exe_result_push,
    output      [4:0]   exe_result_signals,
    
    output      [3:0]   exe_arith_index,
    
    output              exe_arith_sub_carry,
    output              exe_arith_add_carry,
    output              exe_arith_adc_carry,
    output              exe_arith_sbb_carry,
    
    output reg [31:0]   exe_buffer,
    output reg [463:0]  exe_buffer_shifted,
    
    //output local
    output              exe_is_8bit_clear,
    
    output              exe_cmpxchg_switch,
    
    output              exe_task_switch_finished,
    
    output              exe_eip_from_glob_param_2,
    output              exe_eip_from_glob_param_2_16bit,
    
    //branch
    output              exe_branch,
    output      [31:0]  exe_branch_eip
);

//------------------------------------------------------------------------------ 
reg e_invd_code_done;
reg e_invd_data_done;
reg e_wbinvd_code_done;
reg e_wbinvd_data_done;
wire [31:0] exe_buffer_to_reg ;
wire exe_jecxz_condition;
wire [3:0] e_io_allow_bits;
wire exe_int_2_int_trap_same_exception;
wire [7:0] e_aad_result;
wire [1:0] e_cpl_current;
wire [4:0]  e_bit_selector;
wire        e_bit_selected;
wire        e_bit_value;
wire [31:0] e_bit_result;
wire [31:0] e_cr0_reg;
wire        e_cmpxchg_eq;
wire [32:0] e_cmpxchg_sub;
wire [31:0] e_cmpxchg_result;
wire exe_cmd_loop_ecx;
wire exe_cmd_loop_condition;
wire exe_cmd_lar_desc_invalid;
wire exe_cmd_lsl_desc_invalid;
wire exe_cmd_verr_desc_invalid;
wire exe_cmd_verw_desc_invalid;
wire signed [31:0] e_bound_min;
wire signed [31:0] e_bound_max;
wire signed [31:0] e_bound_dst;
wire [31:0] exe_new_tss_max;
wire e_bcd_condition_cf;
wire exe_bcd_condition_af;
wire exe_bcd_condition_cf;
wire [15:0] e_aaa_sum_ax;
wire [15:0] e_aaa_result;
wire [15:0] e_aas_sub_ax;
wire [15:0] e_aas_result;
wire [7:0]  e_daa_sum_low;
wire [7:0]  e_daa_step1;
wire [7:0]  e_daa_sum_high;
wire [7:0]  e_daa_result;
wire [7:0]  e_das_sub_low;
wire [7:0]  e_das_step1;
wire [7:0]  e_das_sub_high;
wire [7:0]  e_das_result;
wire [4:0]  e_bit_scan_forward;
wire        e_bit_scan_zero;
wire [31:0] e_src_ze;
wire [4:0]  e_bit_scan_reverse;
wire cond_0;
wire cond_1;
wire cond_2;
wire cond_3;
wire cond_4;
wire cond_5;
wire cond_6;
wire cond_7;
wire cond_8;
wire cond_9;
wire cond_10;
wire cond_11;
wire cond_12;
wire cond_13;
wire cond_14;
wire cond_15;
wire cond_16;
wire cond_17;
wire cond_18;
wire cond_19;
wire cond_20;
wire cond_21;
wire cond_22;
wire cond_23;
wire cond_24;
wire cond_25;
wire cond_26;
wire cond_27;
wire cond_28;
wire cond_29;
wire cond_30;
wire cond_31;
wire cond_32;
wire cond_33;
wire cond_34;
wire cond_35;
wire cond_36;
wire cond_37;
wire cond_38;
wire cond_39;
wire cond_40;
wire cond_41;
wire cond_42;
wire cond_43;
wire cond_44;
wire cond_45;
wire cond_46;
wire cond_47;
wire cond_48;
wire cond_49;
wire cond_50;
wire cond_51;
wire cond_52;
wire cond_53;
wire cond_54;
wire cond_55;
wire cond_56;
wire cond_57;
wire cond_58;
wire cond_59;
wire cond_60;
wire cond_61;
wire cond_62;
wire cond_63;
wire cond_64;
wire cond_65;
wire cond_66;
wire cond_67;
wire cond_68;
wire cond_69;
wire cond_70;
wire cond_71;
wire cond_72;
wire cond_73;
wire cond_74;
wire cond_75;
wire cond_76;
wire cond_77;
wire cond_78;
wire cond_79;
wire cond_80;
wire cond_81;
wire cond_82;
wire cond_83;
wire cond_84;
wire cond_85;
wire cond_86;
wire cond_87;
wire cond_88;
wire cond_89;
wire cond_90;
wire cond_91;
wire cond_92;
wire cond_93;
wire cond_94;
wire cond_95;
wire cond_96;
wire cond_97;
wire cond_98;
wire cond_99;
wire cond_100;
wire cond_101;
wire cond_102;
wire cond_103;
wire cond_104;
wire cond_105;
wire cond_106;
wire cond_107;
wire cond_108;
wire cond_109;
wire cond_110;
wire cond_111;
wire cond_112;
wire cond_113;
wire cond_114;
wire cond_115;
wire cond_116;
wire cond_117;
wire cond_118;
wire cond_119;
wire cond_120;
wire cond_121;
wire cond_122;
wire cond_123;
wire cond_124;
wire cond_125;
wire cond_126;
wire cond_127;
wire cond_128;
wire cond_129;
wire cond_130;
wire cond_131;
wire cond_132;
wire cond_133;
wire cond_134;
wire cond_135;
wire cond_136;
wire cond_137;
wire cond_138;
wire cond_139;
wire cond_140;
wire cond_141;
wire cond_142;
wire cond_143;
wire cond_144;
wire cond_145;
wire cond_146;
wire cond_147;
wire cond_148;
wire cond_149;
wire cond_150;
wire cond_151;
wire cond_152;
wire cond_153;
wire cond_154;
wire cond_155;
wire cond_156;
wire cond_157;
wire cond_158;
wire cond_159;
wire cond_160;
wire cond_161;
wire cond_162;
wire cond_163;
wire cond_164;
wire cond_165;
wire cond_166;
wire cond_167;
wire cond_168;
wire cond_169;
wire cond_170;
wire cond_171;
wire cond_172;
wire cond_173;
wire cond_174;
wire cond_175;
wire cond_176;
wire cond_177;
wire cond_178;
wire cond_179;
wire cond_180;
wire cond_181;
wire cond_182;
wire cond_183;
wire cond_184;
wire cond_185;
wire cond_186;
wire cond_187;
wire cond_188;
wire cond_189;
wire cond_190;
wire cond_191;
wire cond_192;
wire cond_193;
wire cond_194;
wire cond_195;
wire cond_196;
wire cond_197;
wire cond_198;
wire cond_199;
wire cond_200;
wire cond_201;
wire cond_202;
wire cond_203;
wire cond_204;
wire cond_205;
wire cond_206;
wire cond_207;
wire cond_208;
wire cond_209;
wire cond_210;
wire cond_211;
wire cond_212;
wire cond_213;
wire cond_214;
wire cond_215;
wire cond_216;
wire cond_217;
wire cond_218;
wire cond_219;
wire cond_220;
wire cond_221;
wire cond_222;
wire cond_223;
wire cond_224;
wire cond_225;
wire cond_226;
wire cond_227;
wire cond_228;
wire cond_229;
wire cond_230;
wire cond_231;
wire cond_232;
wire cond_233;
wire cond_234;
wire cond_235;
wire cond_236;
wire cond_237;
wire cond_238;
wire cond_239;
wire cond_240;
wire cond_241;
wire cond_242;
wire cond_243;
wire cond_244;
wire cond_245;
wire cond_246;
wire cond_247;
wire cond_248;
wire cond_249;
wire cond_250;
wire cond_251;
wire cond_252;
wire cond_253;
wire cond_254;
wire cond_255;
wire cond_256;
wire cond_257;
wire cond_258;
wire cond_259;
wire cond_260;
wire cond_261;
wire cond_262;
wire cond_263;
wire cond_264;
wire cond_265;
wire cond_266;
wire cond_267;
wire cond_268;
wire cond_269;
wire cond_270;
wire cond_271;
wire cond_272;
wire cond_273;
wire cond_274;
wire cond_275;
wire cond_276;
wire cond_277;
wire cond_278;
wire cond_279;
wire cond_280;
wire cond_281;
wire cond_282;
wire cond_283;
wire cond_284;
wire cond_285;
wire cond_286;
wire cond_287;
wire cond_288;
wire cond_289;
wire cond_290;
wire cond_291;
wire cond_292;
wire cond_293;
wire cond_294;
wire cond_295;
wire cond_296;
wire cond_297;
wire cond_298;
wire cond_299;

//------------------------------------------------------------------------------ eflags

wire [31:0] exe_push_eflags;
wire [31:0] exe_pushf_eflags;
wire [2:0]  exe_segment;
wire [15:0] exe_selector;
wire [63:0] exe_descriptor;

wire exe_privilege_not_accepted;
wire        exe_buffer_shift;
wire        exe_buffer_shift_word;
wire [32:0] exe_arith_adc;
wire [32:0] exe_arith_add;
wire [31:0] exe_arith_and;
wire [31:0] exe_arith_not;
wire [31:0] exe_arith_or;
wire [32:0] exe_arith_sub;
wire [32:0] exe_arith_sbb;
wire [31:0] exe_arith_xor;

wire        exe_cmpxchg_switch_carry;

wire [15:0] e_seg_by_cmdex;
wire [31:0] e_eip_next_sum;
wire exe_condition;    

assign exe_push_eflags   = { 10'b0,idflag,2'b0,acflag,vmflag,rflag,1'b0,ntflag,iopl,oflag,dflag,iflag,tflag,sflag,zflag,1'b0,aflag,1'b0,pflag,1'b1,cflag };
assign exe_pushf_eflags  = { 10'b0,idflag,2'b0,acflag,1'b0,  1'b0, 1'b0,ntflag,iopl,oflag,dflag,iflag,tflag,sflag,zflag,1'b0,aflag,1'b0,pflag,1'b1,cflag };
                         
//------------------------------------------------------------------------------ descriptor load seg


assign exe_segment    = glob_param_1[18:16];
assign exe_selector   = glob_param_1[15:0];
assign exe_descriptor = glob_descriptor;

//task_switch, lar,lsl,verr,verw
assign exe_privilege_not_accepted =
    exe_selector[`SELECTOR_BITS_RPL] > exe_descriptor[`DESC_BITS_DPL] || cpl > exe_descriptor[`DESC_BITS_DPL];

//------------------------------------------------------------------------------ exe_buffer
    

always @(posedge clk) begin
    if(rst)               exe_buffer_shifted <= 464'd0;
    else if(exe_buffer_shift)       exe_buffer_shifted <= { exe_buffer_shifted[431:0], exe_buffer };
    else if(exe_buffer_shift_word)  exe_buffer_shifted <= { exe_buffer_shifted[447:0], exe_buffer[15:0] };
end

//------------------------------------------------------------------------------ 


assign exe_arith_adc   = src + dst + { 31'd0, cflag };
assign exe_arith_add   = src + dst;
assign exe_arith_and   = src & dst;
assign exe_arith_not   = ~dst;
assign exe_arith_or    = src | dst;
assign exe_arith_sub   = dst - src;
assign exe_arith_sbb   = dst - src - { 31'd0, cflag };
assign exe_arith_xor   = src ^ dst;

assign exe_arith_sub_carry = (exe_cmpxchg_switch)? exe_cmpxchg_switch_carry : exe_arith_sub[32];
assign exe_arith_add_carry = exe_arith_add[32];
assign exe_arith_adc_carry = exe_arith_adc[32];
assign exe_arith_sbb_carry = exe_arith_sbb[32];

//------------------------------------------------------------------------------ 


assign e_seg_by_cmdex =
    (exe_cmdex[2:0] == 3'd0)?   es :
    (exe_cmdex[2:0] == 3'd1)?   cs :
    (exe_cmdex[2:0] == 3'd2)?   ss :
    (exe_cmdex[2:0] == 3'd3)?   ds :
    (exe_cmdex[2:0] == 3'd4)?   fs :
    (exe_cmdex[2:0] == 3'd5)?   gs :
    (exe_cmdex[2:0] == 3'd6)?   ldtr :
                                tr;

//------------------------------------------------------------------------------ task switch

//exe -> microcode
assign task_eip   = (glob_descriptor[`DESC_BITS_TYPE] <= 4'd3)? { 16'd0, exe_buffer_shifted[415:400] } : exe_buffer_shifted[431:400];

//------------------------------------------------------------------------------ Jcc, JCXZ, LOOP


assign e_eip_next_sum =
    (exe_is_8bit)?          exe_eip + { {24{exe_decoder[15]}}, exe_decoder[15:8] } :
    (exe_operand_16bit)?    exe_eip + { {16{exe_decoder[23]}}, exe_decoder[23:8] } :
                            exe_eip + exe_decoder[39:8];

assign exe_branch_eip =
    (exe_operand_16bit)?    { 16'd0, e_eip_next_sum[15:0] } :
                            e_eip_next_sum;

//------------------------------------------------------------------------------ Jcc, SETcc



condition exe_condition_inst(
    .oflag      (oflag), //input
    .cflag      (cflag), //input
    .sflag      (sflag), //input
    .zflag      (zflag), //input
    .pflag      (pflag), //input
    
    .index      (exe_decoder[3:0]), //input [3:0]
    
    .condition  (exe_condition)  //output
);

//------------------------------------------------------------------------------

// synthesis translate_off
wire _unused_ok = &{ 1'b0, edx[31:16], tr_cache[63:44], tr_cache[39:0], exe_mutex_current[9], exe_mutex_current[7:5], exe_mutex_current[3], exe_mutex_current[1],
    exe_decoder[7:4], mult_result[65:64], exe_selector[15:3], exe_descriptor[63:48], exe_descriptor[39:0], e_aaa_sum_ax[7:4], e_aas_sub_ax[7:4], 1'b0 };
// synthesis translate_on

//------------------------------------------------------------------------------

`include "autogen/execute_commands.v"

//------------------------------------------------------------------------------
    
endmodule
