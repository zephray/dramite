/*
 * Copyright (c) 2014, Aleksander Osman
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

`include "../rtl/params.v"

module rtc(
    input               clk,
    input               rst,

    output reg          irq,

    //io slave
    input               io_address,
    input               io_read,
    output reg  [7:0]   io_readdata,
    input               io_write,
    input       [7:0]   io_writedata,

    //mgmt slave
    /*
    128.[26:0]: cycles in second
    129.[12:0]: cycles in 122.07031 us
    */
    input       [7:0]   mgmt_address,
    input               mgmt_write,
    input       [31:0]  mgmt_writedata
);

//------------------------------------------------------------------------------

   localparam [2:0] SEC_UPDATE_START       = 3'd0;
localparam [2:0] SEC_UPDATE_IN_PROGRESS = 3'd1;
localparam [2:0] SEC_SECOND_START       = 3'd2;
localparam [2:0] SEC_SECOND_IN_PROGRESS = 3'd3;
localparam [2:0] SEC_STOPPED            = 3'd4;
reg io_read_last;
reg [26:0] cycles_in_second;
reg [12:0] cycles_in_122us;
reg [2:0] sec_state;
reg [26:0] sec_timeout;
reg update_interrupt;
reg [7:0] rtc_second;
reg [7:0] rtc_minute;
reg [7:0] rtc_hour;
reg [7:0] rtc_dayofweek;
reg [7:0] rtc_dayofmonth;
reg [7:0] rtc_month;
reg [7:0] rtc_year;
reg [7:0] rtc_century;
reg [7:0] alarm_second;
reg [7:0] alarm_minute;
reg [7:0] alarm_hour;
reg alarm_interrupt;
reg crb_freeze;
reg crb_int_periodic_ena;
reg crb_int_alarm_ena;
reg crb_int_update_ena;
reg crb_binarymode;
reg crb_24hour;
reg crb_daylightsaving;
reg [2:0] divider;
reg [3:0] periodic_rate;
reg [12:0] periodic_minor;
reg [12:0] periodic_major;
reg periodic_interrupt;
reg [6:0] ram_address;
   reg [7:0] init_cnt;
   reg [7:0] init_addr;
   reg [7:0] init_data;
   reg       init_flag;
wire io_read_valid;
wire [7:0] io_readdata_next ;
wire interrupt_start;
wire max_second;
wire [7:0] next_second ;
wire max_minute;
wire [7:0] next_minute ;
wire dst_april;
wire dst_october;
wire max_hour;
wire [7:0] next_hour ;
wire max_dayofweek;
wire [7:0] next_dayofweek ;
wire leap_year;
wire max_dayofmonth;
wire [7:0] next_dayofmonth ;
wire max_month;
wire [7:0] next_month ;
wire max_year;
wire [7:0] next_year ;
wire max_century;
wire [7:0] next_century ;
wire rtc_second_update;
wire rtc_minute_update;
wire rtc_hour_update;
wire rtc_day_update;
wire rtc_month_update;
wire rtc_year_update;
wire rtc_century_update;
wire alarm_interrupt_activate;
wire periodic_enabled;
wire periodic_start;
wire periodic_count;
wire [12:0] periodic_major_initial ;
wire [7:0] ram_q;
wire _unused_ok;

always @(posedge clk) begin if(rst) io_read_last <= 1'b0; else if(io_read_last) io_read_last <= 1'b0; else io_read_last <= io_read; end
 assign io_read_valid = io_read/* && io_read_last == 1'b0*/;

//------------------------------------------------------------------------------ cycle count from mgmt

always @(posedge clk) begin
    if(rst)                               cycles_in_second <= `RTC_CYCLES_IN_SECOND;
    else if(mgmt_write && mgmt_address == 8'd128)   cycles_in_second <= mgmt_writedata[26:0];
end

always @(posedge clk) begin
    if(rst)                               cycles_in_122us <= `RTC_CYCLES_IN_122US;
    else if(mgmt_write && mgmt_address == 8'd129)   cycles_in_122us <= mgmt_writedata[12:0];
end

//------------------------------------------------------------------------------ io read

 assign io_readdata_next =
    (io_address == 1'b0)?       8'hFF :
    (ram_address == 7'h00)?     rtc_second :
    (ram_address == 7'h01)?     alarm_second :
    (ram_address == 7'h02)?     rtc_minute :
    (ram_address == 7'h03)?     alarm_second :
    (ram_address == 7'h04)?     rtc_hour :
    (ram_address == 7'h05)?     alarm_hour :
    (ram_address == 7'h06)?     rtc_dayofweek :
    (ram_address == 7'h07)?     rtc_dayofmonth :
    (ram_address == 7'h08)?     rtc_month :
    (ram_address == 7'h09)?     rtc_year :
    (ram_address == 7'h0A)?     { sec_state == SEC_UPDATE_IN_PROGRESS || sec_state == SEC_SECOND_START, divider, periodic_rate } :
    (ram_address == 7'h0B)?     { crb_freeze, crb_int_periodic_ena, crb_int_alarm_ena, crb_int_update_ena,
                                  1'b0, crb_binarymode, crb_24hour, crb_daylightsaving } :
    (ram_address == 7'h0C)?     { irq, periodic_interrupt, alarm_interrupt, update_interrupt, 4'd0 } :
    (ram_address == 7'h0D)?     8'h80 :
    (ram_address == 7'h32)?     rtc_century :
    (ram_address == 7'h37)?     rtc_century :
                                ram_q;

always @(posedge clk) begin
    if(rst)   io_readdata <= 8'd0;
    else                io_readdata <= io_readdata_next;
end

//------------------------------------------------------------------------------ irq

 assign interrupt_start = irq == 1'b0 && (
    (crb_int_periodic_ena && periodic_interrupt) ||
    (crb_int_alarm_ena    && alarm_interrupt) ||
    (crb_int_update_ena   && update_interrupt) );

always @(posedge clk) begin
    if(rst)                                                       irq <= 1'b0;
    else if(io_read_valid && io_address == 1'b1 && ram_address == 7'h0C)    irq <= 1'b0;
    else if(interrupt_start)                                                irq <= 1'b1;
end

//------------------------------------------------------------------------------ once per second state machine



always @(posedge clk) begin
    if(rst)                                                                                   sec_state <= SEC_UPDATE_START;

    else if(crb_freeze || divider[2:1] == 2'b11)                                                        sec_state <= SEC_STOPPED;
    else if(sec_state == SEC_STOPPED)                                                                   sec_state <= SEC_UPDATE_START;

    else if(sec_state == SEC_UPDATE_START)                                                              sec_state <= SEC_UPDATE_IN_PROGRESS;
    else if(sec_state == SEC_UPDATE_IN_PROGRESS && sec_timeout == 27'd0)                                sec_state <= SEC_SECOND_START;
    else if(sec_state == SEC_SECOND_START)                                                              sec_state <= SEC_SECOND_IN_PROGRESS;
    else if(sec_state == SEC_SECOND_IN_PROGRESS && sec_timeout == { 13'd0, cycles_in_122us, 1'b0 })     sec_state <= SEC_UPDATE_START;
end

always @(posedge clk) begin
    if(rst)                               sec_timeout <= 27'd8137; //4069*2 -1
    else if(crb_freeze || divider[2:1] == 2'b11)    sec_timeout <= 27'd8137; //4069*2 -1
    else if(sec_timeout == 27'd0)                   sec_timeout <= cycles_in_second;
    else                                            sec_timeout <= sec_timeout - 27'd1;
end

always @(posedge clk) begin
    if(rst)                                                       update_interrupt <= 1'b0;
    else if(io_read_valid && io_address == 1'b1 && ram_address == 7'h0C)    update_interrupt <= 1'b0;
    else if(sec_state == SEC_SECOND_START)                                  update_interrupt <= 1'b1;
end

//------------------------------------------------------------------------------

 assign max_second =
    (crb_binarymode && rtc_second >= 8'd59) ||
    (~(crb_binarymode) && (rtc_second[7:4] >= 4'd6 || (rtc_second[7:4] == 4'd5 && rtc_second[3:0] >= 4'd9)));

 assign next_second =
    (max_second)?                                       8'd0 :
    (~(crb_binarymode) && rtc_second[3:0] >= 4'd9)?     { rtc_second[7:4] + 4'd1, 4'd0 } :
                                                        rtc_second + 8'd1;

 assign max_minute =
    (crb_binarymode && rtc_minute >= 8'd59) ||
    (~(crb_binarymode) && (rtc_minute[7:4] >= 4'd6 || (rtc_minute[7:4] == 4'd5 && rtc_minute[3:0] >= 4'd9)));

 assign next_minute =
    (max_minute)?                                       8'd0 :
    (~(crb_binarymode) && rtc_minute[3:0] >= 4'd9)?     { rtc_minute[7:4] + 4'd1, 4'd0 } :
                                                        rtc_minute + 8'd1;

 assign dst_april = crb_daylightsaving && rtc_dayofweek == 8'd1 && rtc_month == 8'd4 &&
    ((crb_binarymode && rtc_dayofmonth >= 8'd24) || (~(crb_binarymode) && rtc_dayofmonth[7:4] >= 4'd2 && rtc_dayofmonth[3:0] >= 4'd4)) &&
    rtc_hour == 8'd1;

 assign dst_october = crb_daylightsaving && rtc_dayofweek == 8'd1 &&
    ((crb_binarymode && rtc_month == 8'd10) || (~(crb_binarymode) && rtc_month[7:4] == 4'd1 && rtc_month[3:0] == 4'd0)) &&
    ((crb_binarymode && rtc_dayofmonth >= 8'd25) || (~(crb_binarymode) && rtc_dayofmonth[7:4] >= 4'd2 && rtc_dayofmonth[3:0] >= 4'd5)) &&
    rtc_hour == 8'd1;

 assign max_hour =
    (~(crb_24hour) && crb_binarymode    && rtc_hour[7] && rtc_hour[6:0] >= 7'd12) ||
    (crb_24hour    && crb_binarymode    && rtc_hour >= 8'd23) ||
    (~(crb_24hour) && ~(crb_binarymode) && rtc_hour[7] && (rtc_hour[6:4] >= 3'd2 || (rtc_hour[6:4] == 3'd1 && rtc_hour[3:0] >= 4'd2))) ||
    (crb_24hour    && ~(crb_binarymode) && (rtc_hour[7:4] >= 4'd3 || (rtc_hour[7:4] == 4'd2 && rtc_hour[3:0] >= 4'd3)));

 assign next_hour =
    (dst_april)?                                                                                8'd3 :
    (dst_october)?                                                                              8'd1 :
    (~(crb_24hour) && max_hour)?                                                                8'd1 :
    (crb_24hour    && max_hour)?                                                                8'd0 :
    (~(crb_24hour) && crb_binarymode    && rtc_hour[6:0] >= 7'd12)?                             8'h81 :
    (~(crb_24hour) && ~(crb_binarymode) && rtc_hour[6:4] == 3'd1 && rtc_hour[3:0] >= 4'd2)?     8'h81 :
    (~(crb_24hour) && ~(crb_binarymode) && rtc_hour[6:4] == 3'd0 && rtc_hour[3:0] >= 4'd9)?     { rtc_hour[7], 3'b1, 4'd0 }  :
    (crb_24hour    && ~(crb_binarymode) && rtc_hour[3:0] >= 4'd9)?                              { rtc_hour[7:4] + 4'd1, 4'd0 } :
                                                                                                rtc_hour + 8'd1;

 assign max_dayofweek = rtc_dayofweek >= 8'd7;

 assign next_dayofweek =
    (max_dayofweek)?    8'd1 :
                        rtc_dayofweek + 8'd1;

//simplified leap year condition
 assign leap_year =
    (crb_binarymode    && rtc_year[1:0] == 2'b00) ||
    (~(crb_binarymode) && ((rtc_year[1:0] == 2'b00 && rtc_year[4] == 1'b0) || (rtc_year[1:0] == 2'b10 && rtc_year[4] == 1'b1)));

 assign max_dayofmonth =
    (crb_binarymode && (
        (rtc_month <= 8'd1  && rtc_dayofmonth >= 8'd31) ||
        (rtc_month == 8'd2  && ((~(leap_year) && rtc_dayofmonth >= 8'd28) || (leap_year && rtc_dayofmonth >= 8'd29))) ||
        (rtc_month == 8'd3  && rtc_dayofmonth >= 8'd31) ||
        (rtc_month == 8'd4  && rtc_dayofmonth >= 8'd30) ||
        (rtc_month == 8'd5  && rtc_dayofmonth >= 8'd31) ||
        (rtc_month == 8'd6  && rtc_dayofmonth >= 8'd30) ||
        (rtc_month == 8'd7  && rtc_dayofmonth >= 8'd31) ||
        (rtc_month == 8'd8  && rtc_dayofmonth >= 8'd31) ||
        (rtc_month == 8'd9  && rtc_dayofmonth >= 8'd30) ||
        (rtc_month == 8'd10 && rtc_dayofmonth >= 8'd31) ||
        (rtc_month == 8'd11 && rtc_dayofmonth >= 8'd30) ||
        (rtc_month >= 8'd12 && rtc_dayofmonth >= 8'd31))
    ) ||
    (~(crb_binarymode) && (
        (rtc_month <= 8'h01 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3 && rtc_dayofmonth[3:0] >= 4'd1))) ||
        (rtc_month == 8'h02 && ((~(leap_year) && (rtc_dayofmonth[7:4] >= 4'd3 || (rtc_dayofmonth[7:4] == 4'd2 && rtc_dayofmonth[3:0] >= 4'd8))) ||
                               (leap_year    && (rtc_dayofmonth[7:4] >= 4'd3 || (rtc_dayofmonth[7:4] == 4'd2 && rtc_dayofmonth[3:0] >= 4'd9))))) ||
        (rtc_month == 8'h03 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3 && rtc_dayofmonth[3:0] >= 4'd1))) ||
        (rtc_month == 8'h04 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3))) ||
        (rtc_month == 8'h05 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3 && rtc_dayofmonth[3:0] >= 4'd1))) ||
        (rtc_month == 8'h06 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3))) ||
        (rtc_month == 8'h07 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3 && rtc_dayofmonth[3:0] >= 4'd1))) ||
        (rtc_month == 8'h08 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3 && rtc_dayofmonth[3:0] >= 4'd1))) ||
        (rtc_month == 8'h09 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3))) ||
        (rtc_month == 8'h10 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3 && rtc_dayofmonth[3:0] >= 4'd1))) ||
        (rtc_month == 8'h11 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3))) ||
        (rtc_month >= 8'h12 && (rtc_dayofmonth[7:4] >= 4'd4 || (rtc_dayofmonth[7:4] == 4'd3 && rtc_dayofmonth[3:0] >= 4'd1))))
    );

 assign next_dayofmonth =
    (max_dayofmonth)?                                       8'd1 :
    (~(crb_binarymode) && rtc_dayofmonth[3:0] >= 4'd9)?     { rtc_dayofmonth[7:4] + 4'd1, 4'd0 } :
                                                            rtc_dayofmonth + 8'd1;

 assign max_month =
    (crb_binarymode && rtc_month >= 8'd12) || (~(crb_binarymode) && (rtc_month[7:4] >= 4'd2 || (rtc_month[7:4] == 4'd1 && rtc_month[3:0] >= 4'd2)));

 assign next_month =
    (max_month)?                                    8'd1 :
    (~(crb_binarymode) && rtc_month[3:0] >= 4'd9)?  { rtc_month[7:4] + 4'd1, 4'd0 } :
                                                    rtc_month + 8'd1;

 assign max_year =
    (crb_binarymode && rtc_year >= 8'd99) || (~(crb_binarymode) && (rtc_year[7:4] >= 4'd10 || (rtc_year[7:4] == 4'd9 && rtc_year[3:0] >= 4'd9)));

 assign next_year =
    (max_year)?                                     8'd0 :
    (~(crb_binarymode) && rtc_year[3:0] >= 4'd9)?   { rtc_year[7:4] + 4'd1, 4'd0 } :
                                                    rtc_year + 8'd1;

 assign max_century =
    (crb_binarymode && rtc_century >= 8'd99) || (~(crb_binarymode) && (rtc_century[7:4] >= 4'd10 || (rtc_century[7:4] == 4'd9 && rtc_century[3:0] >= 4'd9)));

 assign next_century =
    (max_century)?                                      8'd0 :
    (~(crb_binarymode) && rtc_century[3:0] >= 4'd9)?    { rtc_century[7:4] + 4'd1, 4'd0 } :
                                                        rtc_century + 8'd1;

//------------------------------------------------------------------------------

 assign rtc_second_update = sec_state == SEC_SECOND_START;
 assign rtc_minute_update = rtc_second_update && max_second;
 assign rtc_hour_update = rtc_minute_update && max_minute;
 assign rtc_day_update = rtc_hour_update   && max_hour;
 assign rtc_month_update = rtc_day_update    && max_dayofmonth;
 assign rtc_year_update = rtc_month_update  && max_month;
 assign rtc_century_update = rtc_year_update   && max_year;

//------------------------------------------------------------------------------

always @(posedge clk) begin
    if(rst)                                                   rtc_second <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h00)                        rtc_second <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h00)     rtc_second <= io_writedata;
    else if(rtc_second_update)                                          rtc_second <= next_second;
end

always @(posedge clk) begin
    if(rst)                                                   rtc_minute <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h02)                        rtc_minute <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h02)     rtc_minute <= io_writedata;
    else if(rtc_minute_update)                                          rtc_minute <= next_minute;
end

always @(posedge clk) begin
    if(rst)                                                   rtc_hour <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h04)                        rtc_hour <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h04)     rtc_hour <= io_writedata;
    else if(rtc_hour_update)                                            rtc_hour <= next_hour;
end

always @(posedge clk) begin
    if(rst)                                                   rtc_dayofweek <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h06)                        rtc_dayofweek <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h06)     rtc_dayofweek <= io_writedata;
    else if(rtc_day_update)                                             rtc_dayofweek <= next_dayofweek;
end

always @(posedge clk) begin
    if(rst)                                                   rtc_dayofmonth <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h07)                        rtc_dayofmonth <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h07)     rtc_dayofmonth <= io_writedata;
    else if(rtc_day_update)                                             rtc_dayofmonth <= next_dayofmonth;
end

always @(posedge clk) begin
    if(rst)                                                   rtc_month <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h08)                        rtc_month <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h08)     rtc_month <= io_writedata;
    else if(rtc_month_update)                                           rtc_month <= next_month;
end

always @(posedge clk) begin
    if(rst)                                                   rtc_year <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h09)                        rtc_year <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h09)     rtc_year <= io_writedata;
    else if(rtc_year_update)                                            rtc_year <= next_year;
end

always @(posedge clk) begin
    if(rst)                                                   rtc_century <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h32)                        rtc_century <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h32)     rtc_century <= io_writedata;
    else if(io_write && io_address == 1'b1 && ram_address == 7'h37)     rtc_century <= io_writedata;
    else if(rtc_century_update)                                         rtc_century <= next_century;
end

//------------------------------------------------------------------------------

always @(posedge clk) begin
    if(rst)                                                   alarm_second <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h01)                        alarm_second <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h01)     alarm_second <= io_writedata;
end

always @(posedge clk) begin
    if(rst)                                                   alarm_minute <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h03)                        alarm_minute <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h03)     alarm_minute <= io_writedata;
end

always @(posedge clk) begin
    if(rst)                                                   alarm_hour <= 8'd0;
    else if(mgmt_write && mgmt_address == 8'h05)                        alarm_hour <= mgmt_writedata[7:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h05)     alarm_hour <= io_writedata;
end

 assign alarm_interrupt_activate =
    (alarm_second[7:6] == 2'b11 || (rtc_second_update && next_second == alarm_second)) &&
    (alarm_minute[7:6] == 2'b11 || (rtc_minute_update && next_minute == alarm_minute) || (~(rtc_minute_update) && rtc_minute == alarm_minute)) &&
    (alarm_hour[7:6] == 2'b11   || (rtc_hour_update && next_hour == alarm_hour)       || (~(rtc_hour_update)   && rtc_hour == alarm_hour));

always @(posedge clk) begin
    if(rst)                                                       alarm_interrupt <= 1'b0;
    else if(io_read_valid && io_address == 1'b1 && ram_address == 7'h0C)    alarm_interrupt <= 1'b0;
    else if(sec_state == SEC_SECOND_START && alarm_interrupt_activate)      alarm_interrupt <= 1'b1;
end

//------------------------------------------------------------------------------

/*
crb_freeze 1: no update, no alarm
*/

always @(posedge clk) begin
    if(rst)                                                   crb_freeze <= 1'b0;
    else if(mgmt_write && mgmt_address == 8'h0B)                        crb_freeze <= mgmt_writedata[7];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h0B)     crb_freeze <= io_writedata[7];
end

always @(posedge clk) begin
    if(rst)                                                   crb_int_periodic_ena <= 1'b0;
    else if(mgmt_write && mgmt_address == 8'h0B)                        crb_int_periodic_ena <= mgmt_writedata[6];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h0B)     crb_int_periodic_ena <= io_writedata[6];
end

always @(posedge clk) begin
    if(rst)                                                   crb_int_alarm_ena <= 1'b0;
    else if(mgmt_write && mgmt_address == 8'h0B)                        crb_int_alarm_ena <= mgmt_writedata[5];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h0B)     crb_int_alarm_ena <= io_writedata[5];
end

always @(posedge clk) begin
    if(rst)                                                   crb_int_update_ena <= 1'b0;
    else if(mgmt_write && mgmt_address == 8'h0B)                        crb_int_update_ena <= ~(mgmt_writedata[7]) & mgmt_writedata[4];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h0B)     crb_int_update_ena <= ~(io_writedata[7]) & io_writedata[4];
end

always @(posedge clk) begin
    if(rst)                                                   crb_binarymode <= 1'b0;
    else if(mgmt_write && mgmt_address == 8'h0B)                        crb_binarymode <= mgmt_writedata[2];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h0B)     crb_binarymode <= io_writedata[2];
end

always @(posedge clk) begin
    if(rst)                                                   crb_24hour <= 1'b1;
    else if(mgmt_write && mgmt_address == 8'h0B)                        crb_24hour <= mgmt_writedata[1];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h0B)     crb_24hour <= io_writedata[1];
end

always @(posedge clk) begin
    if(rst)                                                   crb_daylightsaving <= 1'b0;
    else if(mgmt_write && mgmt_address == 8'h0B)                        crb_daylightsaving <= mgmt_writedata[0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h0B)     crb_daylightsaving <= io_writedata[0];
end

//------------------------------------------------------------------------------

/*
divider 00x : no periodic
divider 11x : no update, no alarm
*/

always @(posedge clk) begin
    if(rst)                                                   divider <= 3'd2;
    else if(mgmt_write && mgmt_address == 8'h0A)                        divider <= mgmt_writedata[6:4];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h0A)     divider <= io_writedata[6:4];
end

always @(posedge clk) begin
    if(rst)                                                   periodic_rate <= 4'd6;
    else if(mgmt_write && mgmt_address == 8'h0A)                        periodic_rate <= mgmt_writedata[3:0];
    else if(io_write && io_address == 1'b1 && ram_address == 7'h0A)     periodic_rate <= io_writedata[3:0];
end

 assign periodic_enabled = divider[2:1] != 2'b00 && periodic_rate != 4'd0;
 assign periodic_start = periodic_enabled && (
                            (periodic_minor == 13'd0 && periodic_major == 13'd0) ||
                            (periodic_minor == 13'd0 && periodic_major == 13'd1));
 assign periodic_count = periodic_enabled && periodic_major >= 13'd1;

 assign periodic_major_initial = {
    periodic_rate == 4'd15, periodic_rate == 4'd14, periodic_rate == 4'd13, periodic_rate == 4'd12,
    periodic_rate == 4'd11, periodic_rate == 4'd10, periodic_rate == 4'd9 || periodic_rate == 4'd2,  periodic_rate == 4'd8 || periodic_rate == 4'd1,
    periodic_rate == 4'd7,  periodic_rate == 4'd6,  periodic_rate == 4'd5,  periodic_rate == 4'd4,
    periodic_rate == 4'd3 };

always @(posedge clk) begin
    if(rst)                                   periodic_minor <= 13'd0;
    else if(~(periodic_enabled))                        periodic_minor <= 13'd0;
    else if(periodic_start)                             periodic_minor <= cycles_in_122us;
    else if(periodic_count && periodic_minor == 13'd0)  periodic_minor <= cycles_in_122us;
    else if(periodic_count)                             periodic_minor <= periodic_minor - 13'd1;
end

always @(posedge clk) begin
    if(rst)                                   periodic_major <= 13'd0;
    else if(~(periodic_enabled))                        periodic_major <= 13'd0;
    else if(periodic_start)                             periodic_major <= periodic_major_initial;
    else if(periodic_count && periodic_minor == 13'd0)  periodic_major <= periodic_major - 13'd1;
end

always @(posedge clk) begin
    if(rst)                                                               periodic_interrupt <= 1'b0;
    else if(io_read_valid && io_address == 1'b1 && ram_address == 7'h0C)            periodic_interrupt <= 1'b0;
    else if(periodic_enabled && periodic_minor == 13'd0 && periodic_major == 13'd1) periodic_interrupt <= 1'b1;
end

//------------------------------------------------------------------------------

always @(posedge clk) begin
    if(rst)                       ram_address <= 7'd0;
    else if(io_write && io_address == 1'b0) ram_address <= io_writedata[6:0];
end

//------------------------------------------------------------------------------


simple_ram #(
    .width      (8),
    .widthad    (7)
)
rtc_ram_inst(
    .clk                (clk),

    .wraddress          ((init_flag) ? init_addr: (mgmt_write && mgmt_address[7] == 1'b0)?   mgmt_address[6:0] : ram_address),
    .wren               ((init_flag) || (mgmt_write && mgmt_address[7] == 1'b0) || (io_write && io_address == 1'b1)),
    .data               ((init_flag) ? init_data : (mgmt_write && mgmt_address[7] == 1'b0)?   mgmt_writedata[7:0] : io_writedata),

    .rdaddress          ((io_write && io_address == 1'b0)?  io_writedata[6:0] : ram_address),
    .q                  (ram_q)
);


always @(posedge clk) begin
   if(rst) begin
      init_flag <= 1;
      init_cnt <= 0;
      init_addr <= 0;
   end else begin
      if(init_flag) begin
   init_cnt <= init_cnt + 1;
   init_addr <= init_cnt;
   case(init_cnt)
     8'h0:
       init_data <= `RTC_SECONDS;
     8'h1:
       init_data <= `RTC_SECONDS_ALARM;
     8'h2:
       init_data <= `RTC_MINUTES;
     8'h3:
       init_data <= `RTC_MINUTES_ALARM;
     8'h4:
       init_data <= `RTC_HOURS;
     8'h5:
       init_data <= `RTC_HOURS_ALARM;
     8'h6:
       init_data <= `RTC_DAYOFWEEK;
     8'h7:
       init_data <= `RTC_DATEDAY;
     8'h8:
       init_data <= `RTC_DATEMONTH;
     8'h9:
       init_data <= `RTC_DATEYEAR;
     8'ha:
       init_data <= `RTC_STATUSREGISTER_A;
     8'hb:
       init_data <= `RTC_STATUSREGISTER_B;
     8'hc:
       init_data <= `RTC_STATUSREGISTER_C;
     8'hd:
       init_data <= `RTC_STATUSREGISTER_D;
     8'he:
       init_data <= `RTC_DIAGNOSTIC_STATUS;
     8'hf:
       init_data <= `RTC_CMOS_SHUTDOWN_STATUS;
     8'h10:
       init_data <= `RTC_FLOPPY_DRIVE_TYPE;
     8'h11:
       init_data <= `RTC_SYSTEM_CONFIG_SETTING;
     8'h12:
       init_data <= `RTC_HDD_TYPE;
     8'h13:
       init_data <= `RTC_TYPEMATIC_PARAMS;
     8'h14:
       init_data <= `RTC_INSTALLED_EQUIPMENTS;
     8'h15:
       init_data <= `RTC_BASEMEMORY_LOW;
     8'h16:
       init_data <= `RTC_BASEMEMORY_HIGH;
     8'h17:
       init_data <= `RTC_EXTMEMORY_LOW;
     8'h18:
       init_data <= `RTC_EXTMEMORY_HIGH;
     8'h19:
       init_data <= `RTC_HDD0_EXTENDEDTYPE;
     8'h1a:
       init_data <= `RTC_HDD1_EXTENDEDTYPE;
     8'h1b:
       init_data <= `RTC_HDD0_CYLINDERS_LOW;
     8'h1c:
       init_data <= `RTC_HDD0_CYLINDERS_HIGH;
     8'h1d:
       init_data <= `RTC_HDD0_HEADS;
     8'h1e:
       init_data <= `RTC_HDD0_WRITE_PRECOMP_LOW;
     8'h1f:
       init_data <= `RTC_HDD0_WRITE_PRECOMP_HIGH;
     8'h20:
       init_data <= `RTC_HDD0_CONTROL_BYTE;
     8'h21:
       init_data <= `RTC_HDD0_LANDING_ZONE_LOW;
     8'h22:
       init_data <= `RTC_HDD0_LANDING_ZONE_HIGH;
     8'h23:
       init_data <= `RTC_HDD0_SPT;
     8'h24:
       init_data <= `RTC_HDD1_CYLINDERS_LOW;
     8'h25:
       init_data <= `RTC_HDD1_CYLINDERS_HIGH;
     8'h26:
       init_data <= `RTC_HDD1_HEADS;
     8'h27:
       init_data <= `RTC_HDD1_WRITE_PRECOMP_LOW;
     8'h28:
       init_data <= `RTC_HDD1_WRITE_PRECOMP_HIGH;
     8'h29:
       init_data <= `RTC_HDD1_CONTROL_BYTE;
     8'h2a:
       init_data <= `RTC_HDD1_LANDING_ZONE_LOW;
     8'h2b:
       init_data <= `RTC_HDD1_LANDING_ZONE_HIGH;
     8'h2c:
       init_data <= `RTC_HDD1_SPT;
     8'h2d:
       init_data <= `RTC_SYSTEM_OPERATIONAL_FLAGS;
     8'h2e:
       init_data <= `RTC_CMOS_CHECKSUM_HIGH;
     8'h2f:
       init_data <= `RTC_CMOS_CHECKSUM_LOW;
     8'h30:
       init_data <= `RTC_MEMSIZE_ABOVE_1M_IN_1K_LOW;
     8'h31:
       init_data <= `RTC_MEMSIZE_ABOVE_1M_IN_1K_HIGH;
     8'h32:
       init_data <= `RTC_IBM_CENTURY;
     8'h33:
       init_data <= `RTC_POST_INFO_FLAGS;
     8'h34:
       init_data <= `RTC_MEMSIZE_ABOVE_16M_IN_64K_LOW;
     8'h35:
       init_data <= `RTC_MEMSIZE_ABOVE_16M_IN_64K_HIGH;
     8'h36:
       init_data <= `RTC_CHIPSET_SPECIFIC_INFO;
     8'h37:
       init_data <= `RTC_IBM_PS2_CENTURY;
     8'h38:
       init_data <= `RTC_ELTORITO_BOOT_SEQUENCE;
     8'h39:
       init_data <= `RTC_ATA_TRANSLATION_POLICY0;
     8'h3a:
       init_data <= `RTC_ATA_TRANSLATION_POLICY1;
     8'h3b:
       init_data <= `RTC_3B;
     8'h3c:
       init_data <= `RTC_3C;
     8'h3d:
       init_data <= `RTC_3D;
     8'h3e:
       init_data <= `RTC_3E;
     8'h3f:
       init_data <= `RTC_3F;
     8'h40:
       init_flag <= 0;
   endcase
      end
   end
end

//------------------------------------------------------------------------------

// synthesis translate_off
 assign _unused_ok = &{ 1'b0, mgmt_writedata[31:27], 1'b0 };
// synthesis translate_on

//------------------------------------------------------------------------------

endmodule
