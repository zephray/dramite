# Dramite

# Overall

This project aims to create a "bridge" that would work with a real 386DX processor. The full system should be IBM-PC compatible, being able to run operating system and software written for IBM-PC. Supporting for other CPUs (like 386SX, 486) and other architecture (like PC-98) are planned, but not now. This project also includes a software x86 emulator for the simulation purpose. Currently work in progress, see Progress for details.

# Build

To run the simulation:

```sh
cd rtl
make
cd ../sim
make
./sim
```

To build for the FPGA board:

Open the project file with ISE/Vivado/Quartus with corresponding FPGA project in the fpga folder. 

# Progress

RTL side:

 - [x] CPU interface
 - [ ] DDR memory controller interface
 - [ ] On-chip / Off-chip cache
 - [ ] 8042-compatible keyboard & mouse controller
 - [ ] 8259-compatible programmable interrupt controller
 - [ ] 8253-compatible programmable interval timer
 - [ ] 8250-compatible UART
 - [ ] MC146818-compatible RTC 
 - [ ] CPU-to-ISA bridge
 - [ ] VGA-compatible graphics

Board-specific:

 - [ ] PCF8583 RTC driver

Verilated Simulator:

 - [x] Basic memory support
 - [ ] x86 CPU simulation
 - [ ] VGA monitor
 - [ ] Serial console

# Acknowledge

This project is heavily based on the ao486 project by Aleksander Osman (https://github.com/alfikpl/ao486) and the Frix project by Arch Laboratory (https://github.com/archlabo/Frix).

This project initially used the tiny x86 cpu emulator by tuz358 (https://github.com/tuz358/cpu-emulator), and later switched to the IBMulator by Marco Bortolin (https://github.com/barotto/IBMulator). This allowed simulation without a real CPU.

This project uses Bochs BIOS and VGABIOS.

The test BIOS is partially derived from the eyalabraham/new-xt-bios, which is licensed under MIT.

# License

The simulation code is licensed under GNU GPLv3 due to dependencies.

The Bochs BIOS and VBIOS code are licensed under GNU LGPLv2.

All other source code including RTL source code are licensed under BSD license. Please check file header for specific copyright information.
